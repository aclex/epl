/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_DMA_H
#define GD32VF103XX_EPL_DMA_H

#include <array>
#include <memory>
#include <span>
#include <stdexcept>
#include <type_traits>

#include <epl/direction.h>

#include <epl/dma/route.h>
#include <epl/dma/mode.h>
#include <epl/dma/priority.h>

#include <epl/dma/endpoint/endpoint.h>
#include <epl/dma/endpoint/spi.h>
#include <epl/dma/endpoint/memory.h>

#include <epl/irq.h>

namespace epl::dma
{
	enum class error : unsigned char
	{
		transfer
	};

	template<std::size_t Part, std::size_t Channel> class channel_stream;

	namespace irq
	{
		template<std::size_t Part, std::size_t Channel> constexpr unsigned int cause;
		template<std::size_t Channel> constexpr unsigned int cause<0, Channel>{30 + Channel};
		template<std::size_t Channel> constexpr unsigned int cause<1, Channel>{75 + Channel};

		namespace event
		{
			class basic_event
			{
			public:
				template<std::size_t Part, std::size_t N> void clear() noexcept
				{

				}
			};

			class half_transfer_finish : public basic_event {};
			class full_transfer_finish : public basic_event {};
		}

		template<std::size_t Part, std::size_t Channel> class basic_channel_handler
		{
			using channel_stream_type = channel_stream<Part, Channel>;

		public:
			static basic_channel_handler* instance() noexcept
			{
				return s_instance.get();
			}

			virtual void on_event(const event::full_transfer_finish& e) noexcept
			{
				channel_stream_type::disable();
			}

			virtual void on_event(const event::half_transfer_finish& e) noexcept {}
			virtual void on_error(const error e) noexcept {}

			inline void resolve() noexcept
			{
				const auto status{pop_status()};

				if (!(status & s_gif_mask))
					return;

				if (status & s_errif_mask)
					on_error(error::transfer);

				if (status & s_htfif_mask)
					on_event(event::half_transfer_finish{});

				if (status & s_ftfif_mask)
					on_event(event::full_transfer_finish{});
			}
			virtual ~basic_channel_handler() {}

		private:
			friend class epl::dma::channel_stream<Part, Channel>;

			static void set_instance(std::unique_ptr<basic_channel_handler>&& s) noexcept
			{
				s_instance = std::move(s);
			}

			template<class T, typename... Args> static void emplace_instance(Args&&... args) noexcept(noexcept(T(std::forward<Args>(args)...)))
			{
				s_instance = std::make_unique<T>(std::forward<Args>(args)...);
			}

			static inline std::uint32_t pop_status() noexcept
			{
				static constexpr auto shift_amount{Channel * 4};

				static constexpr auto select_mask{0xfu << shift_amount};
				static constexpr auto clear_mask{0xeu << shift_amount};

				const auto reg_value{*mcu::register_map::dma::intf<Part>};
				const auto status_unaligned{reg_value & select_mask};

				const auto clear_value{status_unaligned & clear_mask};
				*mcu::register_map::dma::intc<Part> = clear_value;

				const auto status{status_unaligned >> shift_amount};

				return status;
			}

			static inline std::unique_ptr<basic_channel_handler> s_instance;

			static constexpr std::uint32_t s_errif_mask{1 << 3};
			static constexpr std::uint32_t s_htfif_mask{1 << 2};
			static constexpr std::uint32_t s_ftfif_mask{1 << 1};
			static constexpr std::uint32_t s_gif_mask{1};
		};
	}

	template<std::size_t part> static constexpr std::uint32_t clock_flag{};
	template<> constexpr std::uint32_t clock_flag<0>{0b01};
	template<> constexpr std::uint32_t clock_flag<1>{0b10};

	template<std::size_t Part, std::size_t Channel> class channel_stream
	{
	protected:
		static constinit const std::size_t part = Part;
		static constinit const std::size_t channel = Channel;

		static constexpr volatile std::uint32_t* ctl{mcu::register_map::dma::ctl<part, channel>};
		static constexpr volatile std::uint32_t* cnt{mcu::register_map::dma::cnt<part, channel>};
		static constexpr volatile std::uint32_t* paddr{mcu::register_map::dma::paddr<part, channel>};
		static constexpr volatile std::uint32_t* maddr{mcu::register_map::dma::maddr<part, channel>};
		static constexpr volatile std::uint32_t* intc{mcu::register_map::dma::intc<part>};
		static constexpr volatile std::uint32_t* intf{mcu::register_map::dma::intf<part>};

	public:
		using basic_irq_handler_type = irq::basic_channel_handler<part, channel>;

		static inline void reset() noexcept
		{
			*ctl = 0;
			*cnt = 0;
			*paddr = 0;
			*maddr = 0;

			clear_irq();
		}

		static inline void enable(bool disable_on_full_transfer = false) noexcept
		{
			set_bit(ctl, std::uint32_t(1));

			if (disable_on_full_transfer && !basic_irq_handler_type::instance())
			{
				emplace_irq_handling<basic_irq_handler_type>();
			}
		}

		static inline void disable() noexcept
		{
			reset_bit(ctl, std::uint32_t(1));
		}

		static inline void clear_irq() noexcept
		{
			*intc = (1 << (channel * 4));
		}

		static constexpr void enable_irq_handling(std::unique_ptr<basic_irq_handler_type>&& h) noexcept
		{
			basic_irq_handler_type::set_instance(std::move(h));
			enable_irq();
		}

		template<class T, typename... Args> static inline void emplace_irq_handling(Args&&... args) noexcept(noexcept(basic_irq_handler_type::template emplace_instance<T>(std::forward<Args>(args)...)))
		{
			basic_irq_handler_type::template emplace_instance<T>(std::forward<Args>(args)...);
			enable_irq();
		}

		static constexpr void disable_irq_handling() noexcept
		{
			disable_irq();
			basic_irq_handler_type::set_instance(nullptr);
		}

		static inline void reload(const std::size_t count) noexcept
		{
			clear_irq();
			*cnt = count;
		}

		static inline epl::state increment() noexcept
		{
			static constexpr auto mask{static_cast<std::uint32_t>(1 << 7)};
			return get_bit(ctl, mask) ? epl::state::on : epl::state::off;
		}

		template<target_type t = target_type::memory> static inline void set_increment(const bool b) noexcept
		{
			if constexpr (t == target_type::peripheral)
			{
				static constexpr std::uint32_t offset{6};
				static constexpr std::uint32_t mask{1 << offset};
				reset_bit(ctl, mask);
				set_bit(ctl, std::uint32_t(int(b) << offset));
			}
			else
			{
				static constexpr std::uint32_t offset{7};
				static constexpr std::uint32_t mask{1 << offset};
				reset_bit(ctl, mask);
				set_bit(ctl, std::uint32_t(int(b) << offset));
			}
		}

		template<target_type t = target_type::memory> static inline void set_increment(const epl::state st) noexcept
		{
			set_increment<t>(st == state::on);
		}

		template<target_type t = target_type::memory, typename T = std::uint32_t> static inline void set_address(T* address) noexcept
		{
			if constexpr (t == target_type::peripheral)
				*paddr = reinterpret_cast<std::uint32_t>(address);
			else
				*maddr = reinterpret_cast<std::uint32_t>(address);
		}

	protected:
		static inline void enable_clock() noexcept
		{
			constexpr auto bus{mcu::register_map::dma::part_bus<part>};
			rcu::enable_clock<bus, clock_flag<part>>();
		}

		static inline void enable_irq() noexcept
		{
			static constexpr auto cause{dma::irq::cause<part, channel>};

			epl::irq::unit::configure(cause);
			epl::irq::unit::enable(cause);

			set_bit(ctl, s_errie_mask | s_htfie_mask | s_ftfie_mask);
		}

		static inline void disable_irq() noexcept
		{
			epl::irq::unit::disable(dma::irq::cause<part, channel>);
			reset_bit(ctl, s_errie_mask | s_htfie_mask | s_ftfie_mask);
		}

		template<class route> static inline void init_m2m_mode() noexcept
		{
			static constexpr std::uint32_t offset{14};
			static constexpr std::uint32_t mask{1 << offset};
			reset_bit(ctl, mask);

			if constexpr (std::is_same<route, m2m>::value)
			{
				set_bit(ctl, mask);
			}
		}

		static inline void init_priority(const priority pr) noexcept
		{
			load_field_value<12>(ctl, priority_value(pr));
		}

		template<target_type t> static inline void init_width(const request_width w) noexcept
		{
			if constexpr (t == target_type::peripheral)
			{
				load_field_value<8>(ctl, width_value(w));
			}
			else
			{
				load_field_value<10>(ctl, width_value(w));
			}
		}

		static inline void init_mode(const mode m) noexcept
		{
			load_field_value<5>(ctl, static_cast<std::uint32_t>(m == mode::circular));
		}

		template<class route> static inline void init_direction() noexcept
		{
			static constexpr std::uint32_t offset{4};
			static constexpr std::uint32_t mask{1 << offset};
			reset_bit(ctl, mask);

			if constexpr (!std::is_same<route, p2m>::value)
				set_bit(ctl, mask);
		}

		static constexpr std::uint32_t priority_value(const priority pr) noexcept
		{
			switch (pr)
			{
			default:
			case priority::low:
				return 0;

			case priority::medium:
				return 1;

			case priority::high:
				return 2;

			case priority::very_high:
				return 3;
			}
		}

		static constexpr std::uint32_t width_value(const request_width w) noexcept
		{
			switch (w)
			{
			default:
			case request_width::b8:
				return 0;

			case request_width::b16:
				return 1;

			case request_width::b32:
				return 2;
			}
		}

		static constexpr std::uint32_t s_errie_mask{1 << 3};
		static constexpr std::uint32_t s_htfie_mask{1 << 2};
		static constexpr std::uint32_t s_ftfie_mask{1 << 1};
	};

	template<class SourceEndpoint, class DestinationEndpoint> struct stream_traits
	{
		using dir = detect<SourceEndpoint, DestinationEndpoint>; 

		using peripheral_endpoint_type = std::conditional<std::is_same<dir, p2m>::value || std::is_same<dir, m2m>::value, SourceEndpoint, DestinationEndpoint>::type;
		using memory_endpoint_type = std::conditional<std::is_same<dir, m2p>::value, SourceEndpoint, DestinationEndpoint>::type;

		static constinit const direction peripheral_direction{std::is_same<SourceEndpoint, peripheral_endpoint_type>::value ? direction::in : direction::out};

		static constinit const std::size_t part = peripheral_endpoint_type::part;
		static constinit const std::size_t channel = peripheral_endpoint_type::template channel<peripheral_direction>;
	};

	template<basic_endpoint SourceEndpoint, basic_endpoint DestinationEndpoint, class Traits = stream_traits<SourceEndpoint, DestinationEndpoint>> class basic_stream : public channel_stream<Traits::part, Traits::channel>
	{
		using traits_type = Traits;
		using channel_stream_type = channel_stream<traits_type::part, traits_type::channel>;

	public:
		struct irq
		{
			static constexpr unsigned int cause{epl::dma::irq::cause<traits_type::part, traits_type::channel>};
		};

		explicit basic_stream(const bool autostart = true) noexcept
		{
			if (autostart)
				enable();
		}

		template<typename S, typename D> static inline void configure(S* const source_address, D* const destination_address, const epl::dma::mode m = epl::dma::mode::normal, const priority pr = priority::low, const std::size_t count = traits_type::memory_endpoint_type::size) noexcept
		{
			channel_stream_type::reset();

			channel_stream_type::enable_clock();

			channel_stream_type::template init_m2m_mode<typename traits_type::dir>();
			channel_stream_type::init_priority(pr);
			channel_stream_type::template init_width<target_type::peripheral>(traits_type::peripheral_endpoint_type::width());
			channel_stream_type::template init_width<target_type::memory>(traits_type::memory_endpoint_type::width());

			channel_stream_type::template set_increment<target_type::peripheral>(traits_type::peripheral_endpoint_type::increment());
			channel_stream_type::template set_increment<target_type::memory>(traits_type::memory_endpoint_type::increment());
			channel_stream_type::init_mode(m);
			channel_stream_type::template init_direction<typename traits_type::dir>();

			channel_stream_type::template set_address<target_type::peripheral>(source_address);
			channel_stream_type::template set_address<target_type::memory>(destination_address);

			reload(count);
		}

		template<typename D> static inline void configure(D* const destination_address, const epl::dma::mode m = epl::dma::mode::normal, const priority pr = priority::low) noexcept requires addressable_endpoint<typename traits_type::peripheral_endpoint_type>
		{
			basic_stream::template configure(traits_type::peripheral_endpoint_type::address(), destination_address, m, pr);
		}

		static inline void configure(const epl::dma::mode m = epl::dma::mode::normal, const priority pr = priority::low) noexcept requires addressable_endpoint<typename traits_type::peripheral_endpoint_type> && addressable_endpoint<typename traits_type::memory_endpoint_type>
		{
			basic_stream::template configure(traits_type::peripheral_endpoint_type::address(), traits_type::memory_endpoint_type::address(), m, pr);
		}

		static inline void enable(bool disable_on_full_transfer = false) noexcept
		{
			channel_stream_type::enable(disable_on_full_transfer);
			traits_type::peripheral_endpoint_type::template enable<traits_type::peripheral_direction>();
		}

		static inline void disable() noexcept
		{
			channel_stream_type::disable();
			traits_type::peripheral_endpoint_type::template disable<traits_type::peripheral_direction>();
		}

		static inline void reload(const std::size_t count = traits_type::memory_endpoint_type::size) noexcept
		{
			channel_stream_type::reload(count);
		}
	};

	template<class SourceSlot, class DestinationSlot> using stream = basic_stream<endpoint<SourceSlot>, endpoint<DestinationSlot>>;

	template<class Slot>
	class quick_stream
	{
		using resource_slot = Slot;
		template<typename T, std::size_t N> using memory_slot = memory::slot<std::span<T, N>>;

		template<typename T, std::size_t N> using reading_stream_type = stream<resource_slot, memory_slot<T, N>>;
		template<typename T, std::size_t N> using writing_stream_type = stream<memory_slot<T, N>, resource_slot>;

	public:
		template<typename T, std::size_t N> static constexpr reading_stream_type<T, N> read(const std::span<T, N> dest, const mode m = mode::normal, const priority pr = priority::low, const bool autostart = true) noexcept
		{
			using stream_type = reading_stream_type<T, N>;

			stream_type::configure(dest.data(), m, priority::low);

			return stream_type{autostart};
		}

		template<typename T, std::size_t N> static constexpr writing_stream_type<T, N> write(const std::span<T, N> src, const mode m = mode::normal, const priority pr = priority::low, const bool autostart = true) noexcept
		{
			using stream_type = writing_stream_type<T, N>;

			stream_type::configure(src.data(), m, priority::low);

			return stream_type{autostart};
		}
	};
}

#endif // GD32VF103XX_EPL_DMA_H
