/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <string>

#include <epl/thread.h>
#include <epl/gpio.h>
#include <epl/irq.h>

using namespace std;

using namespace epl;

namespace
{
	using led = gpio::pin<gpio::port::b, 0>;
	using button = gpio::pin<gpio::port::a, 0>;

	class irq_handler : public button::basic_irq_handler_type
	{
		void on_event() noexcept override
		{
			button::irq::clear();
		}
	};
}

int main(void)
{
	epl::irq::unit::reset();
	epl::irq::unit::set_level_bits(3);

	epl::irq::unit::enable();

	led::configure(gpio::output_mode::push_pull); // or gpio::configure<led>(...)
	button::configure(gpio::mode::input);

	button::irq::emplace_handling<gpio::irq::edge::falling, irq_handler>();

	while(true)
	{
		led::toggle(); // or gpio::toggle<led>();
		irq::unit::wait_for_interrupt();
	}
}
