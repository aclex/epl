set(EXAMPLE_SPI_SOURCES
	main.cpp
	)

add_executable(spi ${EXAMPLE_SPI_SOURCES})
set_property(TARGET spi PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
target_link_libraries(spi epl)

add_bin_executable(spi)
add_hex_executable(spi)
