/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/gpio.h>
#include <epl/timer.h>

using namespace std;

using namespace stm32::epl;
using namespace stm32::epl::timer;

namespace
{
	constexpr const size_t s_bitlength = 19;
	constexpr const size_t s_duty_length = 7;
	constexpr const size_t frequency = 6 * 1000000;
}

int main(void)
{
	tim<1>::configure
	<
		/* period */ s_bitlength,
		/* prescaler */ mcu::rcc::system_clock_frequency / frequency - 1
	>();

	tim<1>::channel<1, direction::out>::configure
	<
		output_compare_mode::pwm1,
		polarity::active_high,
		gpio::pin<mcu::gpio::PA, 8>
	>();

	tim<1>::channel<1, direction::out>::set_preload(state::on);
	tim<1>::channel<1, direction::out>::set_compare_value(s_duty_length);
	tim<1>::channel<1, direction::out>::enable();
	tim<1>::start();

	while(true);
}
