/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

long __dso_handle = 0;

/* int __attribute__((section(".stub"))) _close(int fd) */
/* { */
	/* return -1; */
/* } */

/* struct stat; */
/* int _fstat(int fd, struct stat* st) */
/* { */
	/* return 0; */
/* } */

/* int _isatty(int fd) */
/* { */
	/* return 0; */
/* } */

/* int _lseek(int fd, int ptr, int dir) */
/* { */
	/* return 0; */
/* } */

/* int __attribute__((section(".stub"))) _getpid() */
/* { */
	/* return 0; */
/* } */

/* int _kill(int pid, int sig) */
/* { */
	/* errno = ENOSYS; */
	/* return -1; */
/* } */

/* unsigned long _read(int fd, void* ptr, unsigned long len) */
/* { */
	/* return 0; */
/* } */

/* int _write(int fd, const void* ptr, unsigned long len) */
/* { */
	/* return 0; */
/* } */

/* int handle_trap() */
/* { */
	/* return 0; */
/* } */
