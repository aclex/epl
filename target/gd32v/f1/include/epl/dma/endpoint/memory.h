/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_DMA_ENDPOINT_MEMORY_H
#define GD32VF103XX_EPL_DMA_ENDPOINT_MEMORY_H

#include <span>
#include <concepts>

namespace epl::dma
{
	namespace memory
	{
		template<class Destination, std::integral auto Part = 0, std::integral auto Channel = 0>
		class slot
		{

		};
	}

	template<typename T, std::size_t N, std::integral auto Part, std::integral auto Channel> class endpoint<memory::slot<std::span<T, N>, Part, Channel>>
	{
	public:
		static constexpr bool is_peripheral{false};
		static constexpr std::size_t part{Part};
		template<direction d> static constexpr std::size_t channel{Channel};
		static constexpr std::size_t size{N};

		static consteval request_width width() noexcept
		{
			constexpr std::size_t numeric_width{sizeof(T) * 8};
			switch (numeric_width)
			{
			default:
			case 8:
				return request_width::b8;

			case 16:
				return request_width::b16;

			case 32:
				return request_width::b32;
			}
		}

		static consteval bool increment() noexcept
		{
			return N > 1;
		}
	};
}

#endif // GD32VF103XX_EPL_DMA_ENDPOINT_MEMORY_H
