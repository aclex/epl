/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_TIM1_H
#define STM32_CPP_EPL_MCU_STM32F0XX_TIM1_H

extern TIM_TypeDef hw_tim_1;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr TIM_TypeDef* peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_tim_1;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::tim1;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb2;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB2ENR_TIM1EN;
		}

		template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::channel_count() noexcept
		{
			return 4;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::tim<1>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> inline timer::count_direction peripherals<mcu_model::stm32f0xx>::tim<1>::current_count_direction() noexcept
		{
			return static_cast<timer::count_direction>(tim::handle->CR1 & (1 << 4) >> 4);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<1>::set_count_direction(timer::count_direction dir) noexcept
		{
			set_bits(tim::handle->CR1, 0b1 << 4, static_cast<std::uint16_t>(dir) << 4);
		}

		template<> template<> inline timer::center_aligned_mode peripherals<mcu_model::stm32f0xx>::tim<1>::current_center_aligned_mode() noexcept
		{
			return static_cast<timer::center_aligned_mode>(tim::handle->CR1 & (0b11 << 5) >> 5);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<1>::set_center_aligned_mode(timer::center_aligned_mode cm) noexcept
		{
			set_bits(tim::handle->CR1, 0b11 << 5, static_cast<std::uint16_t>(cm) << 5);
		}

		template<> template<> inline epl::state peripherals<mcu_model::stm32f0xx>::tim<1>::one_pulse_mode() noexcept
		{
			return static_cast<epl::state>(tim::handle->CR1 & (1 << 3) >> 3);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<1>::set_one_pulse_mode(epl::state st) noexcept
		{
			set_bits(tim::handle->CR1, 0b1 << 3, static_cast<std::uint16_t>(st) << 3);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<1>::enable_outputs() noexcept
		{
			tim::handle->BDTR |= TIM_BDTR_MOE;
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<1>::disable_outputs() noexcept
		{
			tim::handle->BDTR &= ~TIM_BDTR_MOE;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 8:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 9:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch3>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 10:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch4>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 11:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch1n>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 7:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 13:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch2n>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 0:
				case 14:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::ch3n>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 1:
				case 15:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::bkin>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 6:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 12:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<1>::signal_mapper<timer::pin_signal::etr>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 12:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_TIM1_H
