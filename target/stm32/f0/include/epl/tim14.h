/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_TIM14_H
#define STM32_CPP_EPL_MCU_STM32F0XX_TIM14_H

extern TIM_TypeDef hw_tim_14;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr TIM_TypeDef* peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_tim_14;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::tim14;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb1;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB1ENR_TIM14EN;
		}

		template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::channel_count() noexcept
		{
			return 1;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::tim<14>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<14>::signal_mapper<timer::pin_signal::ch1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 4:
				case 7:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 1:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_TIM14_H
