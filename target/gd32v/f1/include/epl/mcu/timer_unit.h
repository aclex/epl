/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_MCU_TIMER_UNIT_H
#define GD32VF103XX_EPL_MCU_TIMER_UNIT_H

#include <n200_timer.h>
#include <n200_func.h>
#include <riscv_encoding.h>

#include <epl/state.h>

namespace epl::mcu::timer_unit
{
	static std::uint64_t load() noexcept
	{
		return *register_map::timer_unit::mtimecmp;
	}

	static void set_load(const std::uint64_t v) noexcept
	{
		*register_map::timer_unit::mtimecmp = v;
	}

	static std::uint64_t value() noexcept
	{
		return *register_map::timer_unit::mtime;
	}

	static void set_value(const std::uint32_t v) noexcept
	{
		register_map::timer_unit::mtime = v;
	}
}

#endif // GD32VF103XX_EPL_MCU_TIMER_UNIT_H
