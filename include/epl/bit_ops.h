/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_BIT_OPS_H
#define EPL_BIT_OPS_H

#include <cstdint>

namespace epl
{
	template<typename T> inline void set_bit(volatile T* const reg, const T mask) noexcept
	{
		*reg = *reg | mask;
	}

	template<typename T> inline void reset_bit(volatile T* const reg, const T mask) noexcept
	{
		*reg = *reg & ~mask;
	}

	template<typename T> inline void init_bit(volatile T* const reg, const T mask, const bool v) noexcept
	{
		v ? set_bit(reg, mask) : reset_bit(reg, mask);
	}

	template<typename T> inline void init_bit(volatile T* const reg, const T mask, const T v) noexcept
	{
		reset_bit(reg, mask);
		set_bit(reg, v);
	}

	template<typename T> inline T get_bit(volatile T* const reg, const T mask) noexcept
	{
		return *reg & mask;
	}

	template<unsigned int offset, typename T = std::uint32_t> inline void load_field_value(volatile T* const reg, const T v) noexcept
	{
		static constexpr std::uint32_t mask{1 << offset};
		reset_bit(reg, mask);
		set_bit(reg, std::uint32_t(v << offset));
	}
}

#endif // EPL_BIT_OPS_H
