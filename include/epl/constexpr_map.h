/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_CONSTEXPR_MAP_H
#define STM32_CPP_EPL_CONSTEXPR_MAP_H

#include <array>
#include <utility>
#include <stdexcept>
#include <cstdint>

namespace stm32
{
	namespace epl
	{
		// inspired by zch's implementation described at
		// https://stackoverflow.com/a/26079954/4297846
		template<typename K, typename V, std::size_t Size> class constexpr_map
		{
			static constexpr std::size_t s_size = Size;

			typedef std::pair<K, V> item_type;
			typedef std::array<item_type, s_size> underlying_type;

		public:
			constexpr explicit constexpr_map(underlying_type content) :
				m_items(content)
			{

			}

			constexpr V at(K key) const
			{
				return at(key, s_size);
			}

			constexpr K key_of(V value) const
			{
				return key_of(value, s_size);
			}

			constexpr std::size_t count(K key) const noexcept
			{
				return count(key, 0, s_size);
			}

			constexpr typename underlying_type::const_iterator cbegin() const noexcept
			{
				return m_items.cbegin();
			}

			constexpr typename underlying_type::const_iterator begin() const noexcept
			{
				return m_items.begin();
			}

			constexpr typename underlying_type::iterator begin() noexcept
			{
				return m_items.begin();
			}

			constexpr typename underlying_type::const_iterator cend() const noexcept
			{
				return m_items.cend();
			}

			constexpr typename underlying_type::const_iterator end() const noexcept
			{
				return m_items.end();
			}

			constexpr typename underlying_type::iterator end() noexcept
			{
				return m_items.end();
			}

			constexpr typename underlying_type::const_iterator crbegin() const noexcept
			{
				return m_items.crbegin();
			}

			constexpr typename underlying_type::const_iterator rbegin() const noexcept
			{
				return m_items.rbegin();
			}

			constexpr typename underlying_type::iterator rbegin() noexcept
			{
				return m_items.rbegin();
			}

			constexpr typename underlying_type::const_iterator crend() const noexcept
			{
				return m_items.crend();
			}

			constexpr typename underlying_type::const_iterator rend() const noexcept
			{
				return m_items.rend();
			}

			constexpr typename underlying_type::iterator rend() noexcept
			{
				return m_items.rend();
			}

		private:
			constexpr V at(K key, std::size_t range) const
			{
				return range ? m_items[range - 1].first == key ? m_items[range - 1].second : at(key, range - 1) : throw std::runtime_error("Element not found.");
			}

			constexpr K key_of(V value, std::size_t range) const
			{
				return range ? m_items[range - 1].second == value ? m_items[range - 1].first : key_of(value, range - 1) : throw std::runtime_error("Element not found.");
			}

			constexpr std::size_t count(K key, std::size_t cnt, std::size_t range) const noexcept
			{
				return range ? m_items[range - 1].first == key ? count(key, cnt + 1, range - 1) : count(key, cnt, range - 1) : cnt;
			}

			underlying_type m_items;
		};
	}
}

#endif // STM32_CPP_EPL_CONSTEXPR_MAP_H
