set(EXAMPLE_USART_IRQ_SOURCES
	main.cpp
	)

# string(TOLOWER ${DEVICE} DEVICE_LOWER)

# set(EXAMPLE_USART_IRQ_SOURCES
# 	main.cpp
# 	${SYSTEM_SOURCES}
# 	${CMAKE_SOURCE_DIR}/src/chrono.cpp
# 	${CMAKE_SOURCE_DIR}/src/bsart1.cpp
# 	${CMAKE_SOURCE_DIR}/src/mcu/${DEVICE_LOWER}/hw_reg.s
# 	)

add_executable(usart_irq ${EXAMPLE_USART_IRQ_SOURCES})
# add_executable(usart_irq ${EXAMPLE_USART_IRQ_SOURCES} $<TARGET_OBJECTS:epl>)
target_link_libraries(usart_irq epl)

add_bin_executable(usart_irq)
add_hex_executable(usart_irq)
