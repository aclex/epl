/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_DMA_ENDPOINT_H
#define GD32VF103XX_EPL_DMA_ENDPOINT_H

#include <epl/direction.h>

namespace epl::dma
{
	template<class Slot> class endpoint
	{
		static_assert("Endpoint specialization failed.");
	};

	template<class T> concept basic_endpoint = requires
	{
		{T::is_peripheral} -> std::convertible_to<bool>;
		{T::part} -> std::convertible_to<std::size_t>;
		{T::template channel<direction{}>} -> std::convertible_to<std::size_t>;
	};

	template<class T> concept addressable_endpoint = requires
	{
		basic_endpoint<T>;

		std::integral<std::decay_t<decltype(*T::address())>>;
		sizeof(*T::address()) <= 4;
	};
}

#endif // GD32VF103XX_EPL_DMA_ENDPOINT_H
