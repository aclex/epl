/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_TIM_H
#define STM32_CPP_EPL_MCU_STM32F0XX_TIM_H

#include <epl/state.h>
#include <epl/util.h>

#include <epl/timer/pin_signal.h>
#include <epl/timer/count_direction.h>
#include <epl/timer/center_aligned_mode.h>
#include <epl/timer/dma.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t no> struct peripherals<mcu_model::stm32f0xx>::tim
		{
			struct constexpr_spec_init_helper
			{
				static constexpr TIM_TypeDef* handle() noexcept;
				static constexpr gpio::af af() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static std::uint32_t clock_flag() noexcept;

				static constexpr std::size_t channel_count() noexcept;
			};

		public:
			static constexpr TIM_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const gpio::af af = constexpr_spec_init_helper::af();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();
			static constexpr const std::size_t channel_count = constexpr_spec_init_helper::channel_count();

			template<timer::pin_signal> struct signal_mapper
			{
				template<gpio::port_name pn, std::size_t pin_no>
				static constexpr bool check() noexcept
				{
					constexpr bool result(find_mapping(pn, pin_no));
					static_assert(result,
						"Unable to connect timer to the requested pin.");
					return result;
				}

				static constexpr bool find_mapping(gpio::port_name pn, std::size_t pin_no) noexcept
				{
					return false;
				}
			};

			// used to override AF of the GPIO in case of collision of timer channels
			template<timer::pin_signal p> struct af_patcher
			{
				template<typename Patcher>
				static constexpr std::uint8_t patch() noexcept
				{
					return 0;
				}
			};

			enum class burst_target : unsigned char
			{
				cr1 = 0x00,
				cr2 = 0x01,
				smcr = 0x02,
				dier = 0x03,
				sr = 0x04,
				egr = 0x05,
				ccmr1 = 0x06,
				ccmr2 = 0x07,
				ccer = 0x08,
				cnt = 0x09,
				psc = 0x0a,
				arr = 0x0b,
				rcr = 0x0c,
				ccr1 = 0x0d,
				ccr2 = 0x0e,
				ccr3 = 0x0f,
				ccr4 = 0x10,
				bdtr = 0x11,
				dcr = 0x12,
				orr = 0x13
			};

			static constexpr timer::count_direction current_count_direction() noexcept
			{
				return timer::count_direction::up;
			}

			static constexpr void set_count_direction(timer::count_direction dir) noexcept
			{
				static_assert(fake_dep<std::integral_constant<std::size_t, no>>::value,
					"Direction setting is not supported for this timer.");
			}

			static constexpr timer::center_aligned_mode current_center_aligned_mode() noexcept
			{
				return timer::center_aligned_mode::edge_aligned;
			}

			static constexpr void set_center_aligned_mode(timer::center_aligned_mode cm) noexcept
			{
				static_assert(fake_dep<std::integral_constant<std::size_t, no>>::value,
					"Center aligned mode option is not supported for this timer.");
			}

			static constexpr epl::state one_pulse_mode() noexcept
			{
				return epl::state::off;
			}

			static constexpr void set_one_pulse_mode(epl::state state) noexcept
			{
				static_assert(fake_dep<std::integral_constant<std::size_t, no>>::value,
					"One pulse mode function is not supported for this timer.");
			}

			static constexpr void enable_outputs() noexcept
			{

			}

			static constexpr void disable_outputs() noexcept
			{

			}

			static constexpr void set_dma_burst_length(std::size_t length) noexcept
			{
				constexpr volatile std::uint16_t& reg(tim::handle->DCR);
				constexpr const std::size_t shift_amount(5);
				constexpr const std::uint32_t mask(0b11111);

				set_bits(reg, mask, shift_amount, length);
			}

			static constexpr void set_dma_burst_target(std::size_t burst_target) noexcept
			{
				constexpr volatile std::uint16_t& reg(tim::handle->DCR);
				constexpr const std::size_t shift_amount(0);
				constexpr const std::uint32_t mask(0b11111);

				set_bits(reg, mask, shift_amount, burst_target);
			}

			static constexpr void set_dma_request(timer::dma::request req, epl::state st) noexcept
			{
				constexpr volatile std::uint16_t& reg(tim::handle->DIER);
				const std::size_t shift_amount(dma_request_shift(req));
				constexpr const std::uint16_t mask(1);
				const std::uint16_t value(static_cast<unsigned char>(st));

				set_bits(reg, mask, shift_amount, value);
			}

			static constexpr std::size_t dma_request_shift(timer::dma::request req)
			{
				switch (req)
				{
				default:
				case timer::dma::request::update:
					return 8;

				case timer::dma::request::cc1:
					return 9;

				case timer::dma::request::cc2:
					return 10;

				case timer::dma::request::cc3:
					return 11;

				case timer::dma::request::cc4:
					return 12;

				case timer::dma::request::trigger:
					return 14;
				}
			}
		};
	}
}

#include "tim1.h"
#include "tim3.h"
#include "tim14.h"
#include "tim15.h"
#include "tim16.h"
#include "tim17.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_TIM_H
