/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_TIMER_H
#define STM32_CPP_EPL_TIMER_H

#include <cstddef>

#include <epl/direction.h>
#include <epl/state.h>

#include <epl/mcu.h>

#include <epl/util.h>

#include <epl/timer/pin_signal.h>

namespace stm32
{
	namespace epl
	{
		namespace timer
		{
			enum class output_compare_mode : unsigned char
			{
				frozen = 0b000,
				active_on_match = 0b001,
				inactive_on_match = 0b010,
				toggle = 0b011,
				force_inactive = 0b100,
				force_active = 0b101,
				pwm1 = 0b110,
				pwm2 = 0b111
			};

			enum class polarity : unsigned char
			{
				active_high = 0,
				active_low = 1
			};

			template<std::size_t no> class tim
			{
			private:
				typedef mcu::tim<no> hw;

			public:
				static constexpr std::size_t tim_no = no;

				template<std::size_t ch_no, direction dir, bool complementary=false>
				struct channel
				{
					static constexpr std::size_t channel_no = ch_no;
					static constexpr epl::direction direction = dir;

					struct burst_target
					{
						static constexpr std::size_t ccr() noexcept
						{
							return static_cast<unsigned char>(tim::hw::burst_target::ccr1) + channel_no - 1;
						}
					};

					static_assert(channel_no >= 0 && channel_no <= tim::hw::channel_count,
						"Incorrect channel number.");

					template
					<
						output_compare_mode ocm,
						polarity p,
						typename Pin,
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr void configure() noexcept
					{
						channel::disable();

						configure_gpio<Pin, channel::signal()>();

						channel::configure_output();
						channel::set_mode<ocm>();
						channel::set_polarity<p>();
					}

					template
					<
						output_compare_mode ocm,
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr void set_mode() noexcept
					{
						volatile std::uint16_t& reg(channel_no > 2 ? tim::hw::handle->CCMR2 : tim::hw::handle->CCMR1);
						constexpr std::size_t shift_amount(4 + shift_amount_8());
						constexpr const std::uint16_t mask(0b111 << shift_amount);

						set_bits(reg, mask, static_cast<std::uint16_t>(ocm) << shift_amount);
					}

					template
					<
						polarity p,
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr void set_polarity() noexcept
					{
						set_bits(tim::hw::handle->CCER, TIM_CCER_CC1P << shift_amount_4(), static_cast<std::uint16_t>(p) << shift_amount_4());
					}

					static constexpr void enable() noexcept
					{
						set_bits(tim::hw::handle->CCER, TIM_CCER_CC1E << shift_amount_4(), 1 << shift_amount_4());

						tim::hw::enable_outputs();
					}

					static constexpr void disable() noexcept
					{
						set_bits(tim::hw::handle->CCER, TIM_CCER_CC1E << shift_amount_4(), 0);
					}

					template
					<
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr void set_preload(state st) noexcept
					{
						volatile std::uint16_t& reg(channel_no > 2 ? tim::hw::handle->CCMR2 : tim::hw::handle->CCMR1);
						constexpr const std::size_t shift_amount(3 + shift_amount_8());

						constexpr const std::uint16_t mask(1 << shift_amount);

						set_bits(reg, mask, static_cast<bool>(st) << shift_amount);
					}

					template
					<
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr std::size_t current_compare_value() noexcept
					{
						return *channel::ccr();
					}

					template
					<
						typename std::enable_if<channel::direction == epl::direction::out, int>::type = 0
					>
					static constexpr void set_compare_value(std::size_t value) noexcept
					{
						*channel::ccr() = value;
					}

				private:
					static constexpr volatile std::uint32_t* ccr() noexcept
					{
						return &(tim::hw::handle->CCR1) + (channel_no - 1);
					}
					static constexpr std::uint16_t shift_amount_4() noexcept
					{
						return 4 * (channel_no - 1);
					}

					static constexpr std::uint16_t shift_amount_8() noexcept
					{
						return 8 * ((channel_no - 1) % 2);
					}

					static constexpr void configure_input() noexcept
					{
						constexpr std::uint16_t& reg(channel_no > 2 ? tim::hw::handle->CCMR2 : tim::hw::handle->CCMR1);
						constexpr const std::uint16_t mask(TIM_CCMR1_CC1S << shift_amount_8());

						set_bits(reg, mask, 1 << shift_amount_8());
					}

					static constexpr void configure_output() noexcept
					{
						volatile std::uint16_t& reg(channel_no > 2 ? tim::hw::handle->CCMR2 : tim::hw::handle->CCMR1);
						constexpr const std::uint16_t mask(TIM_CCMR1_CC1S << shift_amount_8());

						set_bits(reg, mask, 0);
					}

					static constexpr pin_signal signal() noexcept
					{
						if constexpr(complementary)
						{
							return static_cast<pin_signal>(static_cast<unsigned char>(pin_signal::ch1n) + channel_no - 1);
						}
						else
						{
							return static_cast<pin_signal>(static_cast<unsigned char>(pin_signal::ch1) + channel_no - 1);
						}
					}
				};

				struct reg
				{
					static constexpr volatile std::uint16_t* dmar() noexcept
					{
						return &hw::handle->DMAR;
					}

					static constexpr volatile std::uint16_t* cnt() noexcept
					{
						return &hw::handle->CNT;
					}

					static constexpr volatile std::uint16_t* psc() noexcept
					{
						return &hw::handle->PSC;
					}

					static constexpr volatile std::uint16_t* arr() noexcept
					{
						return &hw::handle->ARR;
					}
				};

				template
				<
					std::size_t period,
					std::size_t prescaler
				>
				static constexpr void configure() noexcept
				{
					hw::clock_bus::template enable_clock<hw::clock_flag>();

					set_period<period>();
					set_prescaler<prescaler>();

					update_configuration();
				}

				static constexpr std::size_t current_value() noexcept
				{
					return hw::handle->CNT;
				}

				template<std::size_t value>
				static constexpr void set_value() noexcept
				{
					hw::handle->CNT = value;
				}

				static constexpr std::size_t current_period() noexcept
				{
					return hw::handle->ARR;
				}

				template<std::size_t period>
				static constexpr void set_period() noexcept
				{
					hw::handle->ARR = period;
				}

				static constexpr std::size_t current_prescaler() noexcept
				{
					return hw::handle->PSC;
				}

				template<std::size_t prescaler>
				static constexpr void set_prescaler() noexcept
				{
					hw::handle->PSC = prescaler;
				}

				static constexpr std::size_t current_clock_division() noexcept
				{
					return hw::current_clock_division();
				}

				template<std::size_t clock_division>
				static constexpr void set_clock_division() noexcept
				{
					set_bits(hw::handle->CR1, 0b11 << 8, clock_division_value<clock_division>() << 8);
				}

				static constexpr count_direction current_count_direction() noexcept
				{
					return hw::current_count_direction();
				}

				template<count_direction dir> static constexpr void set_count_direction() noexcept
				{
					hw::set_count_direction(dir);
				}

				static constexpr center_aligned_mode current_center_aligned_mode() noexcept
				{
					return hw::current_center_aligned_mode();
				}

				template<center_aligned_mode m> static constexpr void set_center_aligned_mode() noexcept
				{
					hw::set_center_aligned_mode(m);
				}

				static constexpr state current_one_pulse_mode() noexcept
				{
					return hw::current_one_pulse_mode();
				}

				template<state st> static constexpr void set_one_pulse_mode() noexcept
				{
					hw::set_one_pulse_mode(st);
				}

				static constexpr void set_dma_burst_length(std::size_t length) noexcept
				{
					hw::set_dma_burst_length(length);
				}

				static constexpr void set_dma_burst_target(std::size_t burst_target) noexcept
				{
					hw::set_dma_burst_target(burst_target);
				}

				static constexpr void set_dma_request(timer::dma::request req, epl::state st) noexcept
				{
					hw::set_dma_request(req, st);
				}

				static constexpr void update_configuration() noexcept
				{
					hw::handle->EGR |= 1;
				}

				static constexpr void start() noexcept
				{
					volatile std::uint16_t& reg(hw::handle->CR1);
					constexpr std::uint16_t mask(0x1);
					set_bits(reg, mask, 1);
				}

				static constexpr void stop() noexcept
				{
					constexpr std::uint16_t& reg(hw::handle->CR1);
					constexpr std::uint16_t mask(0x1);
					set_bits(reg, mask, 0);
				}

			private:
				template<std::size_t clock_division>
				static constexpr unsigned char clock_division_value() noexcept
				{
					static_assert(clock_division == 1 || clock_division == 2 || clock_division == 4,
						"The specified value of clock division is not supported.");

					switch(clock_division)
					{
					default:
					case 1:
						return 0;

					case 2:
						return 1;

					case 4:
						return 2;
					}
				}

				template<typename Pin, pin_signal p>
				static constexpr void configure_gpio() noexcept
				{
					if constexpr(!std::is_void<Pin>::value)
								{
									tim::hw::template signal_mapper<p>::template check<Pin::port_name, Pin::pin_no>();

									Pin::template configure
									<
										tim::hw::af,
										gpio::output_type::push_pull,
										gpio::pull::up,
										mcu::gpio::speed::high
									>();

									tim::hw::template af_patcher<p>::template patch<gpio::pin_af_patcher<Pin>>();
								}
				}
			};
		}
	}
}

#endif // STM32_CPP_EPL_TIMER_H
