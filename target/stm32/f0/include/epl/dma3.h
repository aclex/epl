/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_DMA3_H
#define STM32_CPP_EPL_MCU_STM32F0XX_DMA3_H

extern DMA_Channel_TypeDef hw_dma_3;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr DMA_Channel_TypeDef* peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_dma_3;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::ahb;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_AHBENR_DMA1EN;
		}

		template<> template<> constexpr IRQn_Type peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::irq() noexcept
		{
			return DMA1_Channel2_3_IRQn;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::dma<3>::constexpr_spec_init_helper::clock_bus_address()> { };

	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_DMA3_H
