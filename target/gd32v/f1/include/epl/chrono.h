/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_CHRONO_H
#define GD32VF103XX_EPL_CHRONO_H

#include <chrono>
#include <cstdint>
#include <ratio>

namespace epl::this_thread
{
	template<typename Rep, typename Period> void sleep_for(const std::chrono::duration<Rep, Period>&) noexcept;
}

namespace epl::chrono
{
	class system_clock
	{
	public:
		using rep = std::uint64_t;
		using period = std::milli;
		using duration = std::chrono::duration<rep, period>;
		using time_point = std::chrono::time_point<system_clock>;

		static consteval bool is_steady() {return false;}

		static time_point now() noexcept
		{
			start();

			return time_point{duration{s_ticks}};
		}

	private:
		template<typename Rep, typename Period> friend void epl::this_thread::sleep_for(const std::chrono::duration<Rep, Period>&) noexcept;
		friend void timer_unit_tick() noexcept;

		static void start() noexcept;
		static void enable_irq() noexcept;

		static constexpr std::size_t s_irq_number{7};

		static bool s_started;
		static volatile rep s_ticks;
	};
}

#endif // GD32VF103XX_EPL_CHRONO_H
