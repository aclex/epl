/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_TIMER_PIN_SIGNAL_H
#define STM32_CPP_EPL_TIMER_PIN_SIGNAL_H

namespace stm32
{
	namespace epl
	{
		namespace timer
		{
			enum class pin_signal : unsigned char
			{
				ch1 = 1,
				ch2,
				ch3,
				ch4,
				ch1n = 10,
				ch2n,
				ch3n,
				ch4n,
				bkin = 20,
				etr
			};

		}
	}
}

#endif // STM32_CPP_EPL_TIMER_PIN_SIGNAL_H
