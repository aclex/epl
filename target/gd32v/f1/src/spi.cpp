/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/spi.h"

using namespace std;

using namespace epl;
using namespace epl::spi;

namespace
{
	template<size_t N> void pass_irq() noexcept
	{
		auto* const h{spi::irq::basic_port_handler<N>::instance()};

		if (!h)
			return;

		h->resolve();
	}
}

namespace epl::spi::irq
{
	void h0() noexcept
	{
		pass_irq<0>();
	}

	void h1() noexcept
	{
		pass_irq<1>();
	}

	void h2() noexcept
	{
		pass_irq<2>();
	}
}
