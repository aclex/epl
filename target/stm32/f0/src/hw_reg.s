	hw_periph_base = 0x40000000

	hw_apb_periph_base = hw_periph_base
	hw_ahb_periph_base = hw_periph_base + 0x00020000
	hw_ahb2_periph_base = hw_periph_base + 0x08000000

	hw_scs_base = 0xe000e000

# RCC

	.global hw_rcc
	hw_rcc = hw_ahb_periph_base + 0x00001000

# GPIO

	.global hw_gpio_a
	hw_gpio_a = hw_ahb2_periph_base + 0x00000000

	.global hw_gpio_b
	hw_gpio_b = hw_ahb2_periph_base + 0x00000400

	.global hw_gpio_c
	hw_gpio_c = hw_ahb2_periph_base + 0x00000800

	.global hw_gpio_d
	hw_gpio_d = hw_ahb2_periph_base + 0x00000c00

	.global hw_gpio_e
	hw_gpio_e = hw_ahb2_periph_base + 0x00001000

	.global hw_gpio_f
	hw_gpio_f = hw_ahb2_periph_base + 0x00001400

# USART

	.global hw_usart_1
	hw_usart_1 = hw_apb_periph_base + 0x00013800

	.global hw_usart_2
	hw_usart_2 = hw_apb_periph_base + 0x00004400

	.global hw_usart_3
	hw_usart_3 = hw_apb_periph_base + 0x00004800

	.global hw_usart_4
	hw_usart_4 = hw_apb_periph_base + 0x00004c00

	.global hw_usart_5
	hw_usart_5 = hw_apb_periph_base + 0x00005000

	.global hw_usart_6
	hw_usart_6 = hw_apb_periph_base + 0x00011400

# SPI

	.global hw_spi_1
	hw_spi_1 = hw_apb_periph_base + 0x00013000

	.global hw_spi_2
	hw_spi_2 = hw_apb_periph_base + 0x00003800

# TIM

	.global hw_tim_1
	hw_tim_1 = hw_apb_periph_base + 0x00012c00

	.global hw_tim_3
	hw_tim_3 = hw_apb_periph_base + 0x00000400

	.global hw_tim_14
	hw_tim_14 = hw_apb_periph_base + 0x00002000

	.global hw_tim_15
	hw_tim_15 = hw_apb_periph_base + 0x00014000

	.global hw_tim_16
	hw_tim_16 = hw_apb_periph_base + 0x00014400

	.global hw_tim_17
	hw_tim_17 = hw_apb_periph_base + 0x00014800

# DMA

	.global hw_dma_stream_1
	hw_dma_stream_1 = hw_ahb_periph_base

	.global hw_dma_1
	hw_dma_1 = hw_dma_stream_1 + 0x00000008

	.global hw_dma_2
	hw_dma_2 = hw_dma_stream_1 + 0x0000001c

	.global hw_dma_3
	hw_dma_3 = hw_dma_stream_1 + 0x00000030

	.global hw_dma_4
	hw_dma_4 = hw_dma_stream_1 + 0x00000044

	.global hw_dma_5
	hw_dma_5 = hw_dma_stream_1 + 0x00000058

	.global hw_test
	hw_test = 0x40013800

# ADC

	.global hw_adc_common
	hw_adc_common = hw_apb_periph_base + 0x00012708

	.global hw_adc_1
	hw_adc_1 = hw_apb_periph_base + 0x00012400

# SYSTICK

	.global hw_sys_tick
	hw_sys_tick = hw_scs_base + 0x00000010
