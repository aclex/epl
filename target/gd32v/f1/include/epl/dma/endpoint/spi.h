/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_DMA_ENDPOINT_SPI_H
#define GD32VF103XX_EPL_DMA_ENDPOINT_SPI_H

#include <epl/spi.h>

namespace epl::dma
{
	template<std::size_t N, spi::dma::target t, direction d> constexpr std::size_t channel_mapping;

	template<> constexpr std::size_t channel_mapping<0, spi::dma::target::data, direction::in>{1};
	template<> constexpr std::size_t channel_mapping<0, spi::dma::target::data, direction::out>{2};
	template<> constexpr std::size_t channel_mapping<1, spi::dma::target::data, direction::in>{3};
	template<> constexpr std::size_t channel_mapping<1, spi::dma::target::data, direction::out>{4};
	template<> constexpr std::size_t channel_mapping<2, spi::dma::target::data, direction::in>{0};
	template<> constexpr std::size_t channel_mapping<2, spi::dma::target::data, direction::out>{1};

	template<class T> struct target_type_mapping {};

	template<std::size_t N, class... Pins> struct target_type_mapping<spi::port<N, Pins...>>
	{
		using type = spi::dma::target;
		static constexpr type default_value{spi::dma::default_target};
	};

	template<std::size_t N, class... Pins, spi::dma::target Target> class endpoint<spi::dma::slot<spi::port<N, Pins...>, Target>>
	{
	public:
		static constexpr spi::dma::target target{Target};

	private:
		using resource_type = spi::port<N, Pins...>;
		using slot_type = spi::dma::slot<resource_type, target>;

	public:
		static constinit const std::size_t part{N == 2 ? 1 : 0};
		template<direction d> static constinit const std::size_t channel{channel_mapping<N, target, d>};

		static constexpr bool is_peripheral{true};

		static inline request_width width() noexcept
		{
			return slot_type::width();
		}

		static consteval bool increment() noexcept
		{
			return false;
		}

		static consteval volatile std::uint32_t* address() noexcept
		{
			return slot_type::address();
		}

		template<direction d> static inline void enable() noexcept
		{
			slot_type::template enable<d>();
		}

		template<direction d> static inline void disable() noexcept
		{
			slot_type::template disable<d>();
		}
	};
}

#endif // GD32VF103XX_EPL_DMA_ENDPOINT_SPI_H
