set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR riscv)

set(CMAKE_C_COMPILER riscv32-unknown-elf-gcc)
set(CMAKE_CXX_COMPILER riscv32-unknown-elf-g++)

# don't use ${PROJECT_*_DIR} variables, as we can be used before the `project()` command
# don't use also ${CMAKE_*_SOURCE_DIR} variables, as we can be included as subproject
set(LINKER_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../target/${MCU_GEN}/f1/ldscripts)

if (DEFINED MCU_MODEL)
	string(TOUPPER ${MCU_MODEL} UPPER_MCU_MODEL)
	string(SUBSTRING ${UPPER_MCU_MODEL} 0 9 LDSCRIPT_FILENAME_PREFIX)
	string(SUBSTRING ${UPPER_MCU_MODEL} 10 1 LDSCRIPT_FILENAME_SUFFIX)
	string(TOLOWER "${LDSCRIPT_FILENAME_PREFIX}x${LDSCRIPT_FILENAME_SUFFIX}" LDSCRIPT_FILENAME)
else()
	message(FATAL_ERROR "MCU_MODEL variable is necessary.")
endif()

set(LINKER_SCRIPT "${LINKER_SCRIPT_DIR}/${LDSCRIPT_FILENAME}.ld")

# switch off host environment flags settings,
# as they're unlikely suitable for our embedded cross-compilation
unset(ENV{CFLAGS})
unset(ENV{CXXFLAGS})

set(CPU_FLAGS "-march=rv32imac -mabi=ilp32 -mcmodel=medlow")
set(COMMON_FLAGS "${CPU_FLAGS} -ffunction-sections -fdata-sections -gdwarf-2 -nostartfiles")

set(CMAKE_C_FLAGS_INIT ${COMMON_FLAGS})
set(CMAKE_CXX_FLAGS_INIT ${COMMON_FLAGS})
set(CMAKE_ASM_FLAGS_INIT ${CPU_FLAGS})

set(CMAKE_EXE_LINKER_FLAGS_INIT "-Wl,--gc-sections -Wl,--print-memory-usage -specs=nosys.specs -specs=nano.specs -L\"${LINKER_SCRIPT_DIR}\" -T\"${LINKER_SCRIPT}\"")
