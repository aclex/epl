/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F411XE_RCC_H
#define STM32_CPP_EPL_MCU_STM32F411XE_RCC_H

#include <epl/util.h>

extern RCC_TypeDef hw_rcc;

namespace stm32
{
	namespace epl
	{
		template<> struct peripherals<mcu_model::stm32f411xe>::rcc
		{
			static constexpr const RCC_TypeDef* handle = &hw_rcc;

			static constexpr std::size_t system_clock_frequency = 16000000;
			static constexpr std::size_t ahb_divider_shift = 0;
			static constexpr std::size_t apb_divider_shift = 0;

			enum class bus_address : unsigned char
			{
				ahb1,
				apb1,
				apb2
			};

			template<bus_address b> struct bus;

			static constexpr std::size_t ahb_clock_frequency() noexcept
			{
				return system_clock_frequency >> ahb_divider_shift;
			}

			static constexpr std::size_t apb_clock_frequency() noexcept
			{
				return ahb_clock_frequency() >> apb_divider_shift;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::rcc::bus<peripherals<mcu_model::stm32f411xe>::rcc::bus_address::ahb1>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->AHB1ENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->AHB1RSTR |= command_mask;
				RCC->AHB1RSTR &= ~command_mask;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::rcc::bus<peripherals<mcu_model::stm32f411xe>::rcc::bus_address::apb1>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->APB1ENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->APB1RSTR |= command_mask;
				RCC->APB1RSTR &= ~command_mask;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::rcc::bus<peripherals<mcu_model::stm32f411xe>::rcc::bus_address::apb2>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->APB2ENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->APB2RSTR |= command_mask;
				RCC->APB2RSTR &= ~command_mask;
			}
		};
	}
}

#endif // STM32_CPP_EPL_MCU_SMT32F411XE_RCC_H
