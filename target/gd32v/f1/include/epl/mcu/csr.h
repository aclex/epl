/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_MCU_CSR_H
#define GD32VF103XX_EPL_MCU_CSR_H

#include <cstdint>

#include <epl/mcu/register_map.h>
#include <epl/irq/handler.h>

namespace epl::mcu::csr
{
	template<std::uint32_t reg, typename T = std::uint32_t> inline T read() noexcept
	{
		T result;
		asm volatile("csrr %[result], %[reg]" : [result] "=r" (result) : [reg] "i" (reg));

		return result;
	}

	template<std::uint32_t reg, typename T = std::uint32_t> inline void write(const T value) noexcept
	{
		asm volatile("csrw %[reg], %[value]" :: [reg] "i" (reg), [value] "r" (value));
	}

	template<std::uint32_t reg, typename T = std::uint32_t> inline void set(const T mask) noexcept
	{
		asm volatile("csrs %[reg], %[mask]" :: [reg] "i" (reg), [mask] "r" (mask));
	}

	template<std::uint32_t reg, typename T = std::uint32_t> inline void clear(const T mask) noexcept
	{
		asm volatile("csrc %[reg], %[mask]" :: [reg] "i" (reg), [mask] "r" (mask));
	}

	template<bool mie> inline irq::handler_type next_irq() noexcept
	{
		irq::handler_type result;
		if constexpr (mie)
			asm volatile("csrrsi %[result], %[reg], 0x8" : [result] "=r" (result) : [reg] "i" (mcu::register_map::csr::mnxti));
		else
			asm volatile("csrrci %[result], %[reg], 0x8" : [result] "=r" (result) : [reg] "i" (mcu::register_map::csr::mnxti));

		return result;
	}
}

#endif // GD32VF103XX_EPL_MCU_CSR_H
