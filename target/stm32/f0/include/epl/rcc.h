/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_RCC_H
#define STM32_CPP_EPL_MCU_STM32F0XX_RCC_H

#include <chrono>

#include <epl/util.h>

extern RCC_TypeDef hw_rcc;

namespace stm32
{
	namespace epl
	{
		template<> struct peripherals<mcu_model::stm32f0xx>::rcc
		{
			static constexpr const RCC_TypeDef* handle = &hw_rcc;

			static constexpr std::size_t system_clock_frequency = 48000000;

			static constexpr std::size_t ahb_divider_shift = 0;
			static constexpr std::size_t apb_divider_shift = 0;

			enum class bus_address : unsigned char
			{
				ahb,
				apb1,
				apb2
			};

			template<bus_address b> struct bus;

			static constexpr std::size_t ahb_clock_frequency() noexcept
			{
				return system_clock_frequency >> ahb_divider_shift;
			}

			static constexpr std::size_t apb_clock_frequency() noexcept
			{
				return ahb_clock_frequency() >> apb_divider_shift;
			}

			template<typename Rep, typename Period>
			static std::uint64_t to_ticks(const std::chrono::duration<Rep, Period>& duration) noexcept
			{
				const auto microseconds_count { std::chrono::duration_cast<std::chrono::microseconds>(duration).count() };
				const auto ticks_per_us { system_clock_frequency / 1000000 };
				const auto result { microseconds_count * ticks_per_us };

				return result;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::rcc::bus_address::ahb>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->AHBENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->AHBRSTR |= command_mask;
				RCC->AHBRSTR &= ~command_mask;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::rcc::bus_address::apb1>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->APB1ENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->APB1RSTR |= command_mask;
				RCC->APB1RSTR &= ~command_mask;
			}
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::rcc::bus_address::apb2>
		{
			template<std::uint32_t flag_mask> static void enable_clock() noexcept
			{
				RCC->APB2ENR |= flag_mask;
			}

			template<std::uint32_t command_mask> static void reset_sequence() noexcept
			{
				RCC->APB2RSTR |= command_mask;
				RCC->APB2RSTR &= ~command_mask;
			}
		};
	}
}

#endif // STM32_CPP_EPL_MCU_SMT32F0XX_RCC_H
