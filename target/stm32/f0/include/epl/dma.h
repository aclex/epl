/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_DMA_H
#define STM32_CPP_EPL_MCU_STM32F0XX_DMA_H

#include <unordered_map>

#include <epl/state.h>
#include <epl/util.h>

#include <epl/dma/mode.h>
#include <epl/dma/source.h>
#include <epl/dma/priority.h>
#include <epl/dma/interrupt.h>

extern DMA_TypeDef hw_dma_stream_1;

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t no> class peripherals<mcu_model::stm32f0xx>::dma
		{
			struct constexpr_spec_init_helper
			{
				static constexpr DMA_Channel_TypeDef* handle() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static std::uint32_t clock_flag() noexcept;
				static IRQn_Type irq() noexcept;
			};

		public:
			static constexpr DMA_TypeDef* const stream_handle = &hw_dma_stream_1;
			static constexpr DMA_Channel_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();
			static constexpr const IRQn_Type irq = constexpr_spec_init_helper::irq();

			static constexpr epl::dma::mode current_mode() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(5);
				constexpr const std::uint32_t mask(0b1);

				return static_cast<epl::dma::mode>(get_bits<bool>(reg, mask, shift_amount));
			}

			static constexpr void set_mode(epl::dma::mode m) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(5);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(m));
			}

			static constexpr void set_direction(epl::dma::source src, epl::dma::source dest) noexcept
			{
				if (src != dest || src != epl::dma::source::memory)
				{
					set_read_source(src);
				}
				else
				{
					set_memory_to_memory(epl::state::on);
				}
			}

			template<epl::dma::source src, typename T>
			static constexpr void set_address(T* addr) noexcept
			{
				if (dma::enabled())
				{
					return;
				}

				constexpr volatile std::uint32_t& reg(src == epl::dma::source::peripheral ? dma::handle->CPAR : dma::handle->CMAR);
				reg = reinterpret_cast<std::uintptr_t>(addr);

				dma::set_transfer_size<src, sizeof(T)>();
			}

			template<epl::dma::source src, std::size_t transfer_size>
			static constexpr void set_transfer_size() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(src == epl::dma::source::peripheral ? 8 : 10);
				constexpr const std::uint32_t mask(0b11);

				set_bits(reg, mask, shift_amount, transfer_size_value<transfer_size>());
			}

			static constexpr std::size_t number_of_data() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CNDTR);
				constexpr const std::uint32_t mask(0xffff);

				return reg & mask;
			}

			static constexpr void set_number_of_data(std::size_t number) noexcept
			{
				if (dma::enabled())
				{
					return;
				}

				constexpr volatile std::uint32_t& reg(dma::handle->CNDTR);
				constexpr const std::uint32_t mask(0xffff);

				reg |= number & mask;
			}

			template<epl::dma::source src>
			static constexpr void set_increment(epl::state st)
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(src == epl::dma::source::peripheral ? 6 : 7);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(st));
			}

			static constexpr epl::dma::priority current_priority() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(12);
				constexpr const std::uint32_t mask(0b11);

				return static_cast<epl::dma::priority>(get_bits<bool>(reg, mask, shift_amount));
			}

			static constexpr void set_priority(epl::dma::priority pr) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(12);
				constexpr const std::uint32_t mask(0b11);

				set_bits(reg, mask, shift_amount, static_cast<unsigned char>(pr));
			}

			static constexpr bool enabled() noexcept
			{
				return dma::handle->CCR & 1;
			}

			static constexpr void enable() noexcept
			{
				dma::handle->CCR |= 1;
			}

			static constexpr void disable() noexcept
			{
				dma::handle->CCR &= ~0x00000001;
			}

			template<epl::dma::interrupt ir, std::size_t priority>
			static void set_interrupt_handler(void (* const f)()) noexcept
			{
				static_assert(priority >= 1 && priority <= 4, "Incorrect interrupt priority value.");

				set_stream_interrupt_handler<ir, priority>(f);
			}

			static void handle_irq() noexcept
			{
				if (!global_irq_flag())
				{
					return;
				}

				constexpr volatile std::uint32_t& reg(stream_handle->ISR);

				for (const auto& irq_record : s_irq_status_map)
				{
					if (reg & irq_record.first)
					{
						const auto& f(s_int_handlers.find(irq_record.second));
						if (f != s_int_handlers.end())
						{
							f->second();
						}

						clear_irq_flag(irq_record.second);
					}
				}

				clear_global_irq_flag();
			}

		private:
			static constexpr void set_read_source(epl::dma::source src) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(4);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(src));
			}

			static constexpr void set_memory_to_memory(epl::state st) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(14);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(st));
			}

			template<std::size_t transfer_size>
			static constexpr std::uint32_t transfer_size_value() noexcept
			{
				static_assert(transfer_size == 1 || transfer_size == 2 || transfer_size == 4,
					"Only quarter, half or whole 32-bit word transfer sizes are supported.");

				switch (transfer_size)
				{
				case 1:
					return 0;

				case 2:
					return 1;

				case 4:
					return 2;
				}

				// should never come here
				return 0;
			}

			template<epl::dma::interrupt ir, std::size_t priority>
			static void set_stream_interrupt_handler(void (* const f)()) noexcept
			{
				s_int_handlers[ir] = f;

				switch_interrupt_source<state::on, ir>();

				NVIC_EnableIRQ(irq);
				NVIC_SetPriority(irq, priority);
			}

			template<epl::state st, epl::dma::interrupt ir> static void switch_interrupt_source() noexcept
			{
				static constexpr const epl::constexpr_map<epl::dma::interrupt, std::uint32_t, 3> irq_map(
				{{
					{ epl::dma::interrupt::half_transfer, DMA_CCR_HTIE },
					{ epl::dma::interrupt::transfer_complete, DMA_CCR_TCIE },
					{ epl::dma::interrupt::transfer_error, DMA_CCR_TEIE }
				}});

				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::uint32_t mask(DMA_CCR_HTIE | DMA_CCR_TCIE | DMA_CCR_TEIE);

				set_bits(reg, mask, irq_map.at(ir));
			}

			static void clear_irq_flag(const epl::dma::interrupt ir) noexcept
			{
				constexpr volatile std::uint32_t& reg(stream_handle->IFCR);

				set_bit(reg, s_irq_clear_map.at(ir));
			}

			static bool global_irq_flag() noexcept
			{
				constexpr volatile std::uint32_t& reg(stream_handle->ISR);
				constexpr volatile std::uint32_t mask(DMA_ISR_GIF1 << s_stream_shift);

				return reg & mask;
			}

			static void clear_global_irq_flag() noexcept
			{
				constexpr volatile std::uint32_t& reg(stream_handle->IFCR);
				set_bit(reg, DMA_IFCR_CGIF1 << s_stream_shift);
			}

			inline static constexpr std::size_t s_stream_shift { 4 * (no - 1) };

			inline static std::unordered_map<epl::dma::interrupt, void (*)()> s_int_handlers;

			static constexpr const epl::constexpr_map<std::uint32_t, epl::dma::interrupt, 3> s_irq_status_map
			{{{
				{ DMA_ISR_HTIF1 << s_stream_shift, epl::dma::interrupt::half_transfer },
				{ DMA_ISR_TCIF1 << s_stream_shift, epl::dma::interrupt::transfer_complete },
				{ DMA_ISR_TEIF1 << s_stream_shift, epl::dma::interrupt::transfer_error }
			}}};

			static constexpr const epl::constexpr_map<epl::dma::interrupt, std::uint32_t, 3> s_irq_clear_map
			{{{
				{ epl::dma::interrupt::half_transfer, DMA_IFCR_CHTIF1 << s_stream_shift },
				{ epl::dma::interrupt::transfer_complete, DMA_IFCR_CTCIF1 << s_stream_shift },
				{ epl::dma::interrupt::transfer_error, DMA_IFCR_CTEIF1 << s_stream_shift }
			}}};
		};
	}
}

#include "dma1.h"
#include "dma2.h"
#include "dma3.h"
#include "dma4.h"
#include "dma5.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_DMA_H
