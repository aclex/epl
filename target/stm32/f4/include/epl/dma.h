/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_DMA_H
#define STM32_CPP_EPL_MCU_STM32F0XX_DMA_H

#include <epl/state.h>
#include <epl/util.h>

#include <epl/dma/mode.h>
#include <epl/dma/source.h>
#include <epl/dma/priority.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t no> class peripherals<mcu_model::stm32f0xx>::dma
		{
			struct constexpr_spec_init_helper
			{
				static constexpr DMA_Channel_TypeDef* handle() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static std::uint32_t clock_flag() noexcept;
			};

		public:
			static constexpr DMA_Channel_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();

			static constexpr epl::dma::mode current_mode() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(5);
				constexpr const std::uint32_t mask(0b1);

				return static_cast<epl::dma::mode>(get_bits<bool>(reg, mask, shift_amount));
			}

			static constexpr void set_mode(epl::dma::mode m) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(5);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(m));
			}

			static constexpr void set_direction(epl::dma::source src, epl::dma::source dest) noexcept
			{
				if (src != dest || src != epl::dma::source::memory)
				{
					set_read_source(src);
				}
				else
				{
					set_memory_to_memory(epl::state::on);
				}
			}

			template<epl::dma::source src, typename T>
			static constexpr void set_address(T* addr) noexcept
			{
				if (dma::enabled())
				{
					return;
				}

				constexpr volatile std::uint32_t& reg(src == epl::dma::source::peripheral ? dma::handle->CPAR : dma::handle->CMAR);
				reg = reinterpret_cast<std::uintptr_t>(addr);

				dma::set_transfer_size<src, sizeof(T)>();
			}

			template<epl::dma::source src, std::size_t transfer_size>
			static constexpr void set_transfer_size() noexcept
			{
				static_assert(transfer_size == 1 || transfer_size == 2 || transfer_size == 4,
					"Only quarter, half or whole 32-bit word transfer sizes are supported.");

				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(src == epl::dma::source::peripheral ? 8 : 10);
				constexpr const std::uint32_t mask(0b11);

				set_bits(reg, mask, shift_amount, transfer_size);
			}

			static constexpr std::size_t number_of_data() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CNDTR);
				constexpr const std::uint32_t mask(0xffff);

				return reg & mask;
			}

			static constexpr void set_number_of_data(std::size_t number) noexcept
			{
				if (dma::enabled())
				{
					return;
				}

				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::uint32_t mask(0b1);

				reg |= number & mask;
			}

			template<epl::dma::source src>
			static constexpr void set_increment(epl::state st)
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(src == epl::dma::source::peripheral ? 6 : 7);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(st));
			}

			static constexpr epl::dma::priority current_priority() noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(12);
				constexpr const std::uint32_t mask(0b11);

				return static_cast<epl::dma::priority>(get_bits<bool>(reg, mask, shift_amount));
			}

			static constexpr void set_priority(epl::dma::priority pr) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(12);
				constexpr const std::uint32_t mask(0b11);

				set_bits(reg, mask, shift_amount, static_cast<unsigned char>(pr));
			}

			static constexpr bool enabled() noexcept
			{
				return dma::handle->CCR & 1;
			}

			static constexpr void enable() noexcept
			{
				dma::handle->CCR |= 1;
			}

			static constexpr void disable() noexcept
			{
				dma::handle->CCR &= ~0x00000001;
			}

		private:
			static constexpr void set_read_source(epl::dma::source src) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr const std::size_t shift_amount(4);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(src));
			}

			static constexpr void set_memory_to_memory(epl::state st) noexcept
			{
				constexpr volatile std::uint32_t& reg(dma::handle->CCR);
				constexpr std::size_t shift_amount(14);
				constexpr const std::uint32_t mask(0b1);

				set_bits(reg, mask, shift_amount, static_cast<bool>(st));
			}
		};
	}
}

#include "dma1.h"
#include "dma2.h"
#include "dma3.h"
#include "dma4.h"
#include "dma5.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_DMA_H
