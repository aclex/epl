/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_IRQ_H
#define GD32VF103XX_EPL_IRQ_H

#include <epl/bit_ops.h>

#include <epl/mcu/csr.h>

namespace epl::irq
{
	enum class mode : unsigned char
	{
		non_vector,
		vector
	};

	enum class trigger : unsigned char
	{
		level = 0,
		front = 2,
		rear = 3
	};

	class unit
	{
	public:
		class context_holder
		{
		public:
			context_holder() noexcept;
			unsigned int cause() const noexcept;
			~context_holder();

		private:
			const std::uint32_t m_mepc;
			const std::uint32_t m_mcause;
			const std::uint32_t m_msubm;
		};

		static constexpr std::uint32_t mie{1 << 3};

		static void reset() noexcept;

		static inline void configure(const unsigned int cause, const mode m = mode::non_vector, const std::uint8_t level = 1, const std::uint8_t priority = 0, const trigger t = trigger::level) noexcept
		{
			set_mode_and_trigger(cause, m, t);
			set_priority_and_level(cause, priority, level);
		}

		static inline void enable() noexcept
		{
			epl::mcu::csr::set<epl::mcu::register_map::csr::mstatus>(mie);
		}

		static inline void enable(const unsigned int cause, const mode m = mode::non_vector) noexcept
		{
			set_bit(mcu::register_map::eclic::int_ie(cause), std::uint8_t{1});
			set_bit(mcu::register_map::eclic::int_attr(cause), static_cast<std::uint8_t>(m));
		}

		static inline void disable() noexcept
		{
			epl::mcu::csr::clear<epl::mcu::register_map::csr::mstatus>(mie);
		}

		static inline void disable(const unsigned int cause) noexcept
		{
			reset_bit(mcu::register_map::eclic::int_ie(cause), std::uint8_t{1});
			reset_bit(mcu::register_map::eclic::int_attr(cause), std::uint8_t{1});
		}

		static inline void wait_for_interrupt() noexcept
		{
			asm volatile ("wfi");
		}

		static inline enum mode mode(const unsigned int cause) noexcept
		{
			return irq::mode{static_cast<unsigned char>(*mcu::register_map::eclic::int_attr(cause) & 1)};
		}

		static inline enum trigger trigger(const unsigned int cause) noexcept
		{
			return irq::trigger{static_cast<unsigned char>(*mcu::register_map::eclic::int_attr(cause) & 0b111 >> 1)};
		}

		static inline std::size_t ctl_bits() noexcept
		{
			static constexpr std::uint32_t offset{21};
			static constexpr std::uint32_t mask{0b111 << offset};
			return get_bit<const std::uint32_t>(mcu::register_map::eclic::info, mask) >> offset;
		}

		static inline std::size_t level_bits() noexcept
		{
			static constexpr std::uint8_t offset{1};
			static constexpr std::uint8_t num_interrupt_mask{0b111 << offset};
			return get_bit(mcu::register_map::eclic::cfg, num_interrupt_mask) >> offset;
		}

		static inline void set_level_bits(const std::size_t b) noexcept
		{
			static constexpr std::uint8_t offset{1};
			const std::uint8_t mask{static_cast<std::uint8_t>(b << offset)};
			return set_bit(mcu::register_map::eclic::cfg, mask);
		}

		static inline std::size_t num_interrupt() noexcept
		{
			static constexpr std::uint32_t mask{0xfff};
			return get_bit<const std::uint32_t>(mcu::register_map::eclic::info, mask);
		}

		static inline std::size_t version() noexcept
		{
			static constexpr std::uint32_t offset{13};
			static constexpr std::uint32_t mask{0xff << offset};
			return get_bit<const std::uint32_t>(mcu::register_map::eclic::info, mask) >> offset;
		}

		static inline std::uint8_t main_level_threshold() noexcept
		{
			return *mcu::register_map::eclic::mth;
		}

		static inline void set_main_level_threshold(const std::uint8_t value) noexcept
		{
			*mcu::register_map::eclic::mth = value;
		}

		static inline std::uint8_t priority(const unsigned int cause) noexcept
		{
			const auto total_bits{ctl_bits()};
			const auto remaining_bits{8 - total_bits};
			const auto lvl_bits{level_bits()};
			const auto priority_bits{total_bits - lvl_bits};
			const auto mask{static_cast<std::uint8_t>((0xff >> (8 - priority_bits)) << remaining_bits)};

			return get_bit(mcu::register_map::eclic::int_ctl(cause), mask) >> remaining_bits;
		}

		static inline std::uint8_t level(const unsigned int cause) noexcept
		{
			const auto remaining_bits{8 - level_bits()};
			const auto mask{static_cast<std::uint8_t>(0xff << remaining_bits)};

			return get_bit(mcu::register_map::eclic::int_ctl(cause), mask) >> remaining_bits;
		}

	private:
		static inline void set_mode_and_trigger(const unsigned int cause, const irq::mode m, const irq::trigger t) noexcept
		{
			const auto attr{static_cast<std::uint8_t>((static_cast<unsigned char>(t) << 1) | static_cast<unsigned char>(m))};

			*mcu::register_map::eclic::int_attr(cause) = attr;
		}

		static inline void set_priority_and_level(const unsigned int cause, const std::uint8_t priority, const std::uint8_t level) noexcept
		{
			const auto total_bits{ctl_bits()};
			const auto lvl_bits{level_bits()};
			const auto priority_bits{total_bits - lvl_bits};
			const auto priority_mask{0xff >> (8 - priority_bits)};
			const auto level_mask{0xff >> (8 - lvl_bits)};

			const auto level_part{(level & level_mask) << priority_bits};
			const auto priority_part{(priority & priority_mask)};
			const auto level_priority{(level_part | priority_part) << (8 - total_bits)};
			const auto ctlbits{static_cast<std::uint8_t>(level_priority | (0xff >> total_bits))};

			*mcu::register_map::eclic::int_ctl(cause) = ctlbits;
		}

		static constexpr std::size_t s_default_no_level_bits{3};
	};
}

#endif // GD32VF103XX_EPL_IRQ_H
