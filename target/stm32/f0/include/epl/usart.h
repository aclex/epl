/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_USART_H
#define STM32_CPP_EPL_MCU_STM32F0XX_USART_H

#include <unordered_map>

#include <epl/constexpr_map.h>
#include <epl/state.h>

#include <epl/io/usart/pin_signal.h>
#include <epl/io/usart/parity.h>
#include <epl/io/usart/interrupt.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t port_no> struct peripherals<mcu_model::stm32f0xx>::usart<port_no>
		{
		private:
			struct constexpr_spec_init_helper
			{
				static constexpr USART_TypeDef* handle() noexcept;
				static constexpr gpio::af af() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static std::uint32_t clock_flag() noexcept;
			};

			static constexpr std::uint16_t s_9bit_mask = 0x01ff;

		public:
			static constexpr USART_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const gpio::af af = constexpr_spec_init_helper::af();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();

			template<io::usart::pin_signal> struct signal_mapper
			{
				template<gpio::port_name pn, std::size_t pin_no>
				static constexpr bool check() noexcept
				{
					constexpr bool result(find_mapping(pn, pin_no));
					static_assert(result,
						"Unable to connect the specified signal to the requested pin.");
					return result;
				}

				static constexpr bool find_mapping(gpio::port_name pn, std::size_t pin_no) noexcept
				{
					return false;
				}
			};

			static constexpr bool supports_flow_control() noexcept
			{
				return port_no != 5 && port_no != 6;
			}

			static void enable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_UE);

				set_bits(reg, mask, USART_CR1_UE);
			}

			static void disable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_UE);

				set_bits(reg, mask, 0);
			}

			static std::uint16_t read() noexcept
			{
				return handle->RDR & s_9bit_mask;
			}

			static void write(const std::uint16_t v) noexcept
			{
				handle->TDR = v & s_9bit_mask;
			}

			template<std::size_t word_length> static constexpr void set_word_length() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_M);

				set_bits(reg, mask, word_length_value<word_length>());
			}

			template<epl::io::usart::parity par> static constexpr void set_parity() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_PCE | USART_CR1_PS);

				set_bits(reg, mask, parity_value<par>());
			}

			template<typename RXPin, typename TXPin> static constexpr void set_mode() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_RE | USART_CR1_TE);

				set_bits(reg, mask, mode_value<RXPin, TXPin>());
			}

			template<typename StopBitsRatio> static constexpr void set_stop_bits() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR2);
				constexpr const std::uint32_t mask(USART_CR2_STOP);

				set_bits(reg, mask, stop_bits_value<StopBitsRatio>());
			}

			template<typename RTSPin, typename CTSPin> static constexpr void set_flow_control() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR3);
				constexpr const std::uint32_t mask(USART_CR3_RTSE | USART_CR3_CTSE);

				set_bits(reg, mask, flow_control_value<RTSPin, CTSPin>());
			}

			template<std::size_t baud_rate> static constexpr void set_overdrive() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_OVER8);

				set_bits(reg, mask, overdrive_value<baud_rate>());
			}

			template<std::size_t baud_rate> static constexpr void set_speed() noexcept
			{
				handle->BRR = speed_value<baud_rate>();
			}

			static constexpr void wait_for_ready_to_receive() noexcept
			{
				while (!(handle->ISR & USART_ISR_RXNE));
			}

			static constexpr void wait_for_ready_to_send() noexcept
			{
				while (!(handle->ISR & USART_ISR_TXE));
			}

			static constexpr void wait_for_transfer_complete() noexcept
			{
				while (!(handle->ISR & USART_ISR_TC));
			}

			template<io::usart::interrupt ir, std::size_t priority = 1> static void set_interrupt_handler(void (* const f)()) noexcept
			{
				static_assert(priority >= 1 && priority <= 4, "Incorrect interrupt priority value.");

				set_port_interrupt_handler(ir, priority, f);
			}

			template<io::usart::interrupt ir> static void reset_interrupt_handler()
			{
				s_int_handlers.erase(ir);
				switch_interrupt_source<state::off>(ir);
			}

			static void handle_irq() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->ISR);

				for (const auto& irq_record : s_irq_status_map)
				{
					if (reg & irq_record.first)
					{
						const auto& f(s_int_handlers.find(irq_record.second));
						if (f != s_int_handlers.end())
						{
							f->second();
						}
					}
				}
			}

		private:
			template<typename StopBitsRatio> static constexpr std::uint32_t stop_bits_value() noexcept
			{
				constexpr float stop_bits(StopBitsRatio::num / StopBitsRatio::den);
				constexpr std::size_t shift_amount { 12 };

				static_assert(stop_bits == 1 || stop_bits == 2, "Only 1 or 2 stop bits is supported by STM32F0xx.");

				switch (StopBitsRatio::num)
				{
				default:
				case 1:
					return 0b00 << shift_amount;

				case 2:
					return 0b10 << shift_amount;
				}
			}

			template<std::size_t word_length> static constexpr std::uint32_t word_length_value() noexcept
			{
				static_assert(word_length == 8 || word_length == 9, "Word lengths of 8 and 9 bits are only supported.");

				switch (word_length)
				{
				default:
				case 8:
					return 0;

				case 9:
					return USART_CR1_M;
				}
			}

			template<epl::io::usart::parity par> static constexpr std::uint32_t parity_value() noexcept
			{
				switch (par)
				{
				default:
				case epl::io::usart::parity::no:
					return 0;

				case epl::io::usart::parity::even:
					return USART_CR1_PCE;

				case epl::io::usart::parity::odd:
					return USART_CR1_PCE | USART_CR1_PS;
				}
			}

			template<typename RXPin, typename TXPin> static constexpr std::uint32_t mode_value() noexcept
			{
				return USART_CR1_RE * (!std::is_void<RXPin>::value) | USART_CR1_TE * (!std::is_void<TXPin>::value);
			}

			template<typename RTSPin, typename CTSPin> static constexpr std::uint32_t flow_control_value() noexcept
			{
				return USART_CR3_RTSE * (!std::is_void<RTSPin>::value) | USART_CR3_CTSE * (!std::is_void<CTSPin>::value);
			}

			template<std::size_t baud_rate> static constexpr std::uint32_t overdrive_value() noexcept
			{
				if (baud_rate <= 3000000)
				{
					return 0;
				}
				else
				{
					return USART_CR1_OVER8;
				}
			}

			template<std::size_t baud_rate> static constexpr std::uint16_t speed_value() noexcept
			{
				constexpr bool low_oversample(overdrive_value<baud_rate>());

				if (!low_oversample)
				{
					return real_clock_frequency<baud_rate>();
				}
				else
				{
					const std::uint16_t result(real_clock_frequency<baud_rate>() * 2);
					return (result & ~0b111) | ((result & 0b111) >> 1);
				}
			}

			template<std::size_t baud_rate> static constexpr std::size_t real_clock_frequency() noexcept
			{
				static_assert(baud_rate > 0, "Baud rate shall be positive.");
				return clock_frequency() / baud_rate + ((clock_frequency() % baud_rate) > baud_rate / 2);
			}

			static constexpr std::size_t clock_frequency() noexcept
			{
				return rcc::apb_clock_frequency();
			}

			static void set_port_interrupt_handler(const io::usart::interrupt ir, const std::size_t priority, void (* const f)()) noexcept
			{
				static_assert(fake_dep<usart<port_no>>::value, "Interrupts are not supported for this port.");
			}

			template<epl::state st> static void switch_interrupt_source(const io::usart::interrupt ir) noexcept
			{
				constexpr const epl::constexpr_map<io::usart::interrupt, std::uint32_t, 11> irq_map(
					{{
						{ io::usart::interrupt::txe, USART_CR1_TXEIE },
						{ io::usart::interrupt::cts, USART_CR3_CTSIE },
						{ io::usart::interrupt::tc, USART_CR1_TCIE },
						{ io::usart::interrupt::rxne, USART_CR1_RXNEIE },
						{ io::usart::interrupt::idle, USART_CR1_IDLEIE },
						{ io::usart::interrupt::pe, USART_CR1_PEIE },
						{ io::usart::interrupt::ne, USART_CR3_EIE },
						{ io::usart::interrupt::ore, USART_CR3_EIE },
						{ io::usart::interrupt::fe, USART_CR3_EIE },
						{ io::usart::interrupt::cmf, USART_CR1_CMIE },
						{ io::usart::interrupt::rtof, USART_CR1_RTOIE }
					}});

				switch (ir)
				{
				case io::usart::interrupt::txe:
				case io::usart::interrupt::tc:
				case io::usart::interrupt::rxne:
				case io::usart::interrupt::idle:
				case io::usart::interrupt::pe:
				case io::usart::interrupt::cmf:
				case io::usart::interrupt::rtof:
				{
					constexpr volatile std::uint32_t& reg(handle->CR1);
					const std::uint32_t mask(irq_map.at(ir));

					set_bits(reg, mask, static_cast<bool>(st) ? mask : 0);
				}

				case io::usart::interrupt::cts:
				case io::usart::interrupt::ne:
				case io::usart::interrupt::ore:
				case io::usart::interrupt::fe:
				{
					constexpr volatile std::uint32_t& reg(handle->CR3);
					const std::uint32_t mask(irq_map.at(ir));

					set_bits(reg, mask, static_cast<bool>(st) ? mask : 0);
				}
				}
			}

			friend void USART1_IRQHandler() noexcept;

			inline static std::unordered_map<io::usart::interrupt, void (*)()> s_int_handlers;

			static constexpr const epl::constexpr_map<std::uint32_t, io::usart::interrupt, 11> s_irq_status_map
			{{{
				{ USART_ISR_TXE, io::usart::interrupt::txe },
				{ USART_ISR_CTS, io::usart::interrupt::cts },
				{ USART_ISR_TC, io::usart::interrupt::tc },
				{ USART_ISR_RXNE, io::usart::interrupt::rxne },
				{ USART_ISR_IDLE, io::usart::interrupt::idle },
				{ USART_ISR_PE, io::usart::interrupt::pe },
				{ USART_ISR_NE, io::usart::interrupt::ne },
				{ USART_ISR_ORE, io::usart::interrupt::ore },
				{ USART_ISR_FE, io::usart::interrupt::fe },
				{ USART_ISR_CMF, io::usart::interrupt::cmf },
				{ USART_ISR_RTOF, io::usart::interrupt::rtof }
			}}};
		};
	}
}

#include "usart1.h"
#include "usart2.h"
#include "usart3.h"
#include "usart4.h"
#include "usart5.h"
#include "usart6.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_USART_H
