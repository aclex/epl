add_executable(sanity main.cpp)
target_link_libraries(sanity epl)

add_bin_executable(sanity)
add_hex_executable(sanity)
