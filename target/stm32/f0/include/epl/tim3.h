/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_TIM3_H
#define STM32_CPP_EPL_MCU_STM32F0XX_TIM3_H

extern TIM_TypeDef hw_tim_3;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr TIM_TypeDef* peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_tim_3;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::tim3;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb1;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB1ENR_TIM3EN;
		}

		template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::channel_count() noexcept
		{
			return 4;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::tim<3>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> inline timer::count_direction peripherals<mcu_model::stm32f0xx>::tim<3>::current_count_direction() noexcept
		{
			return static_cast<timer::count_direction>(tim::handle->CR1 & (1 << 4) >> 4);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<3>::set_count_direction(timer::count_direction dir) noexcept
		{
			set_bits(tim::handle->CR1, 0b1 << 4, static_cast<std::uint16_t>(dir) << 4);
		}

		template<> template<> inline timer::center_aligned_mode peripherals<mcu_model::stm32f0xx>::tim<3>::current_center_aligned_mode() noexcept
		{
			return static_cast<timer::center_aligned_mode>(tim::handle->CR1 & (0b11 << 5) >> 5);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<3>::set_center_aligned_mode(timer::center_aligned_mode cm) noexcept
		{
			set_bits(tim::handle->CR1, 0b11 << 5, static_cast<std::uint16_t>(cm) << 5);
		}

		template<> template<> inline epl::state peripherals<mcu_model::stm32f0xx>::tim<3>::one_pulse_mode() noexcept
		{
			return static_cast<epl::state>(tim::handle->CR1 & (1 << 3) >> 3);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<3>::set_one_pulse_mode(epl::state st) noexcept
		{
			set_bits(tim::handle->CR1, 0b1 << 3, static_cast<std::uint16_t>(st) << 3);
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<3>::signal_mapper<timer::pin_signal::ch1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 6:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 4:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 6:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<3>::signal_mapper<timer::pin_signal::ch2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 7:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 5:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 7:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<3>::signal_mapper<timer::pin_signal::ch3>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 0:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 8:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<3>::signal_mapper<timer::pin_signal::ch4>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 1:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 9:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<3>::signal_mapper<timer::pin_signal::etr>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PD:
				switch (pin_no)
				{
				case 2:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_TIM3_H
