/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

namespace
{
	enum class scoped_enum_t
	{
		w0 = 0,
		w1
	};

	int arg_a{}, arg_b{};

	scoped_enum_t arg_e = scoped_enum_t::w0;

	void on_static_event(const scoped_enum_t l) noexcept
	{
		if (arg_e == scoped_enum_t::w0)
		{
			++arg_a;
		}
		else
		{
			++arg_b;
		}

		if (l == scoped_enum_t::w0)
		{
			++arg_a;
		}
		else
		{
			++arg_b;
		}
	}

	class test_class
	{
	public:
		explicit test_class() noexcept :
			m_arg_e(scoped_enum_t::w0)
		{

		}

		void on_event() noexcept
		{
			if (m_arg_e == scoped_enum_t::w0)
			{
				++m_arg_a;
			}
			else
			{
				++m_arg_b;
			}
		}

	private:
		int m_arg_a{}, m_arg_b{};
		scoped_enum_t m_arg_e;
	};

}

int main(void)
{
	on_static_event(arg_e);

	test_class tc;
	tc.on_event();
}
