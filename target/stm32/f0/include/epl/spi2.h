/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_SPI2_H
#define STM32_CPP_EPL_MCU_STM32F0XX_SPI2_H

extern SPI_TypeDef hw_spi_2;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr SPI_TypeDef* peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_spi_2;
		}

	template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::spi2;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb1;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB1ENR_SPI2EN;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::reset_command() noexcept
		{
			return RCC_APB1RSTR_SPI2RST;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::spi<2>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::spi<2>::signal_mapper<io::spi::pin_signal::mosi>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 15:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 3:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::spi<2>::signal_mapper<io::spi::pin_signal::miso>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 14:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 2:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::spi<2>::signal_mapper<io::spi::pin_signal::sck>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 10:
				case 13:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::spi<2>::signal_mapper<io::spi::pin_signal::nss>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 9:
				case 13:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_SPI2_H
