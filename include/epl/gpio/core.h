/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_GPIO_H
#define EPL_GPIO_H

#include <epl/state.h>
#include <epl/direction.h>

#include <epl/gpio/mode.h>
#include <epl/gpio/push_pull.h>
#include <epl/gpio/output_mode.h>

namespace epl::gpio
{
	template<typename... Pins, typename... Args> constexpr void configure(Args... args) noexcept
	{
		(Pins::configure(args...), ...);
	}
}

#endif // EPL_GPIO_H
