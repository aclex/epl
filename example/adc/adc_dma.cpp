/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/chrono.h>

#include <epl/io/usart.h>

#include <epl/adc/adc.h>
#include <epl/dma.h>

using namespace std;

using namespace stm32::epl;
using namespace stm32::epl::io::usart;
using namespace stm32::epl::adc;

namespace
{
	typedef stm32::epl::chrono::steady_clock mcu_clock;
	typedef stm32::epl::adc::adc<1> my_adc;

	typedef gpio::pin<mcu::gpio::PA, 7> adc_pin1;
	typedef gpio::pin<mcu::gpio::PA, 6> adc_pin2;

	typedef gpio::pin<mcu::gpio::PA, 10> rx_pin;
	typedef gpio::pin<mcu::gpio::PA, 9> tx_pin;

	typedef usart<1, rx_pin, tx_pin> usart1;

	array<uint16_t, 2> buffer =
	{
		2, 2
	};
}

int main(void)
{
	mcu_clock::start();

	usart1::configure<115200, 8, 1, parity::no>();
	usart1::enable();

	my_adc::configure();
	my_adc::calibrate();
	my_adc::enable();
	my_adc::set_active_channels<adc_pin1, adc_pin2>();
	my_adc::set_dma_request<state::on>();

	// ADC update DMA request is routed to DMA channel 1
	dma::stream<1>::configure<dma::source::peripheral, dma::source::memory, dma::mode::circular>();
	dma::stream<1>::set_address<dma::source::peripheral>(my_adc::reg::dr());
	dma::stream<1>::set_address<dma::source::memory>(buffer.data());
	dma::stream<1>::set_transfer_size<dma::source::peripheral, 2>();
	dma::stream<1>::set_transfer_size<dma::source::memory, 2>();
	dma::stream<1>::set_number_of_data(buffer.size());

	dma::stream<1>::set_increment<dma::source::peripheral>(state::off);
	dma::stream<1>::set_increment<dma::source::memory>(state::on);

	dma::stream<1>::enable();

	dma::stream<1>::set_interrupt_handler<dma::interrupt::transfer_complete>(
		[]()
		{
			for (const auto b : buffer)
				usart1() << to_string(b) << ' ';

			usart1() << "\r\n";
		});


	while(true)
	{
		usart1() << "starting conversion\r\n";
		dma::stream<1>::enable();
		my_adc::start_conversion();

		mcu_clock::sleep_for(1s);
	}
}
