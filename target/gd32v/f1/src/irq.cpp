/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/irq.h"

#include <array>

#include "epl/mcu/register_map.h"
#include "epl/mcu/csr.h"

#include "epl/gpio.h"

using namespace std;

using namespace epl;
using namespace epl::irq;
using namespace epl::mcu::csr;

namespace epl
{
	namespace chrono
	{
		extern void timer_unit_tick() noexcept;
	}

	namespace spi::irq
	{
		extern void h0() noexcept;
		extern void h1() noexcept;
		extern void h2() noexcept;
	}

	namespace dma::irq
	{
		extern void h00() noexcept;
		extern void h01() noexcept;
		extern void h02() noexcept;
		extern void h03() noexcept;
		extern void h04() noexcept;
		extern void h05() noexcept;
		extern void h06() noexcept;
		extern void h10() noexcept;
		extern void h11() noexcept;
		extern void h12() noexcept;
		extern void h13() noexcept;
		extern void h14() noexcept;
	}

	namespace gpio::irq
	{
		extern void h0() noexcept;
		extern void h1() noexcept;
		extern void h2() noexcept;
		extern void h3() noexcept;
		extern void h4() noexcept;
		extern void h9_5() noexcept;
		extern void h15_10() noexcept;
	}
}

namespace
{
	using handler = void (*)();

	const size_t g_irq_count{87};
	const array<handler, g_irq_count> g_irq_table
	{
		/* 0*/ nullptr,
		/* 1*/ nullptr,
		/* 2*/ nullptr,
		/* 3*/ nullptr,
		/* 4*/ nullptr,
		/* 5*/ nullptr,
		/* 6*/ nullptr,
		/* 7*/ epl::chrono::timer_unit_tick,
		/* 8*/ nullptr,
		/* 9*/ nullptr,
		/*10*/ nullptr,
		/*11*/ nullptr,
		/*12*/ nullptr,
		/*13*/ nullptr,
		/*14*/ nullptr,
		/*15*/ nullptr,
		/*16*/ nullptr,
		/*17*/ nullptr,
		/*18*/ nullptr,
		/*19*/ nullptr,
		/*20*/ nullptr,
		/*21*/ nullptr,
		/*22*/ nullptr,
		/*23*/ nullptr,
		/*24*/ nullptr,
		/*25*/ gpio::irq::h0,
		/*26*/ gpio::irq::h1,
		/*27*/ gpio::irq::h2,
		/*28*/ gpio::irq::h3,
		/*29*/ gpio::irq::h4,
		/*30*/ dma::irq::h00,
		/*31*/ dma::irq::h01,
		/*32*/ dma::irq::h02,
		/*33*/ dma::irq::h03,
		/*34*/ dma::irq::h04,
		/*35*/ dma::irq::h05,
		/*36*/ dma::irq::h06,
		/*37*/ nullptr,
		/*38*/ nullptr,
		/*39*/ nullptr,
		/*40*/ nullptr,
		/*41*/ nullptr,
		/*42*/ gpio::irq::h9_5,
		/*43*/ nullptr,
		/*44*/ nullptr,
		/*45*/ nullptr,
		/*46*/ nullptr,
		/*47*/ nullptr,
		/*48*/ nullptr,
		/*49*/ nullptr,
		/*50*/ nullptr,
		/*51*/ nullptr,
		/*52*/ nullptr,
		/*53*/ nullptr,
		/*54*/ spi::irq::h0,
		/*55*/ spi::irq::h1,
		/*56*/ nullptr,
		/*57*/ nullptr,
		/*58*/ nullptr,
		/*59*/ gpio::irq::h15_10,
		/*60*/ nullptr,
		/*61*/ nullptr,
		/*62*/ nullptr,
		/*63*/ nullptr,
		/*64*/ nullptr,
		/*65*/ nullptr,
		/*66*/ nullptr,
		/*67*/ nullptr,
		/*68*/ nullptr,
		/*69*/ nullptr,
		/*70*/ spi::irq::h2,
		/*71*/ nullptr,
		/*72*/ nullptr,
		/*73*/ nullptr,
		/*74*/ nullptr,
		/*75*/ dma::irq::h10,
		/*76*/ dma::irq::h11,
		/*77*/ dma::irq::h12,
		/*78*/ dma::irq::h13,
		/*79*/ dma::irq::h14,
		/*80*/ nullptr,
		/*81*/ nullptr,
		/*82*/ nullptr,
		/*83*/ nullptr,
		/*84*/ nullptr,
		/*85*/ nullptr,
		/*86*/ nullptr
	};

	template<typename T> T extract_cause(const T v) noexcept
	{
		return v & 0xfff;
	}

	unsigned int read_cause() noexcept
	{
		return extract_cause(mcu::csr::read<mcu::register_map::csr::mcause>());
	}
}

extern "C" __attribute__((interrupt("machine")))
void exception_handler_trap()
{
	const auto cause{mcu::csr::read<mcu::register_map::csr::mcause>()};
}

extern "C" __attribute__((interrupt)) __attribute__((aligned(4)))
void non_vectorized_interrupt_handler()
{
	epl::irq::unit::context_holder cxt;

	while (mcu::csr::next_irq<true>())
	{
		const auto cause{read_cause()};
		const auto h{g_irq_table[cause]};
		if (h)
			h();
	}
}

void unit::reset() noexcept
{
	*mcu::register_map::eclic::cfg = 0;
	*mcu::register_map::eclic::mth = 0;

	const size_t num_interrupts{*mcu::register_map::eclic::info & 0x1fff};

	for (auto i = 0; i < num_interrupts; ++i)
	{
		*mcu::register_map::eclic::int_ip(i) = 0;
		*mcu::register_map::eclic::int_ie(i) = 0;
		*mcu::register_map::eclic::int_attr(i) = 0;
		*mcu::register_map::eclic::int_ctl(i) = 0;
	}
}

unit::context_holder::context_holder() noexcept :
	m_mepc(mcu::csr::read<mcu::register_map::csr::mepc>()),
	m_mcause(mcu::csr::read<mcu::register_map::csr::mcause>()),
	m_msubm(mcu::csr::read<mcu::register_map::csr::msubm>())
{

}

unsigned int unit::context_holder::cause() const noexcept
{
	return extract_cause(m_mcause);
}

unit::context_holder::~context_holder()
{
	unit::disable();

	mcu::csr::write<mcu::register_map::csr::mepc>(m_mepc);
	mcu::csr::write<mcu::register_map::csr::mcause>(m_mcause);
	mcu::csr::write<mcu::register_map::csr::msubm>(m_msubm);
}
