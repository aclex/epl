/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_H
#define STM32_CPP_EPL_MCU_STM32F0XX_H

#include <cstddef>

#include <stm32f0xx.h>

#include <epl/mcu.h>

namespace stm32
{
	namespace epl
	{
		template<mcu_model m> class peripherals
		{
		public:
			struct rcc;
			struct sys_tick;
			struct gpio;
			template<std::size_t no> class usart;
			template<std::size_t no> class spi;
			template<std::size_t no> class adc;
			template<std::size_t no> class tim;
			template<std::size_t no> class dma;
		};

		template<> struct basic_mcu<mcu_model::stm32f0xx> : public peripherals<mcu_model::stm32f0xx>
		{

		};

		using mcu = basic_mcu<mcu_model::stm32f0xx>;
	}
}

// include headers with peripherals specializations to not grow the file too long

#include "stm32f0xx/rcc.h"
#include "stm32f0xx/gpio.h"
#include "stm32f0xx/usart.h"
#include "stm32f0xx/spi.h"
#include "stm32f0xx/tim.h"
#include "stm32f0xx/dma.h"
#include "stm32f0xx/adc.h"
#include "stm32f0xx/sys_tick.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_H
