/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_TIMER_CENTER_ALIGNED_MODE_H
#define STM32_CPP_EPL_TIMER_CENTER_ALIGNED_MODE_H


namespace stm32
{

	namespace epl
	{
		namespace timer
		{
			enum class center_aligned_mode : unsigned char
			{
				edge_aligned,
				m1,
				m2,
				m3
			};
		}
	}
}

#endif // STM32_CPP_EPL_TIMER_CENTER_ALIGNED_MODE_H
