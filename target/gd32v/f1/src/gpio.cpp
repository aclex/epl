/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/gpio.h"

using namespace std;

using namespace epl;
using namespace epl::gpio;

namespace
{
	template<size_t Number> void pass_irq() noexcept
	{
		auto* const h{gpio::irq::basic_channel_handler<Number>::instance()};

		if (!h)
			return;

		h->resolve();
	}
}

namespace epl::gpio::irq
{
	void h0() noexcept
	{
		pass_irq<0>();
	}

	void h1() noexcept
	{
		pass_irq<1>();
	}

	void h2() noexcept
	{
		pass_irq<2>();
	}

	void h3() noexcept
	{
		pass_irq<3>();
	}

	void h4() noexcept
	{
		pass_irq<4>();
	}

	void h9_5() noexcept
	{
		pass_irq<5>();
		pass_irq<6>();
		pass_irq<7>();
		pass_irq<8>();
		pass_irq<9>();
	}

	void h15_10() noexcept
	{
		pass_irq<10>();
		pass_irq<11>();
		pass_irq<12>();
		pass_irq<13>();
		pass_irq<14>();
		pass_irq<15>();
	}
}
