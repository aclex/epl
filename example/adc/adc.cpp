/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <epl/chrono.h>

#include <epl/io/usart.h>

#include <epl/adc/adc.h>

using namespace std;

using namespace stm32::epl;
using namespace stm32::epl::io::usart;
using namespace stm32::epl::adc;

namespace
{
	typedef stm32::epl::chrono::steady_clock mcu_clock;
	typedef stm32::epl::adc::adc<1> my_adc;

	typedef gpio::pin<mcu::gpio::PA, 7> adc_pin;

	typedef gpio::pin<mcu::gpio::PA, 10> rx_pin;
	typedef gpio::pin<mcu::gpio::PA, 9> tx_pin;

	typedef usart<1, rx_pin, tx_pin> usart1;
}

int main(void)
{
	mcu_clock::start();

	usart1::configure<115200, 8, 1, parity::no>();

	my_adc::configure();
	my_adc::calibrate();
	my_adc::enable();
	my_adc::set_active_channels<adc_pin>();

	while(true)
	{
		my_adc::start_conversion();

		while (my_adc::ongoing_conversion());

		usart1() << to_string(my_adc::value()) << "\r\n";

		mcu_clock::sleep_for(1s);
	}
}
