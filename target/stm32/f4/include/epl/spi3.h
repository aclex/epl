/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F411XE_SPI3_H
#define STM32_CPP_EPL_MCU_STM32F411XE_SPI3_H

extern SPI_TypeDef hw_spi_3;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr SPI_TypeDef* peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_spi_3;
		}

	template<> template<> constexpr peripherals<mcu_model::stm32f411xe>::gpio::af peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::spi3;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f411xe>::rcc::bus_address peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb1;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB1ENR_SPI3EN;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::reset_command() noexcept
		{
			return RCC_APB1RSTR_SPI3RST;
		}

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f411xe>::rcc::bus<peripherals<mcu_model::stm32f411xe>::spi<3>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f411xe>::spi<3>::signal_mapper<io::spi::pin_signal::mosi>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 5:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 12:
					return true;
				}

			case gpio::PD:
				switch (pin_no)
				{
				case 6:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f411xe>::spi<3>::signal_mapper<io::spi::pin_signal::miso>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 4:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 11:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f411xe>::spi<3>::signal_mapper<io::spi::pin_signal::sck>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PB:
				switch (pin_no)
				{
				case 3:
				case 12:
					return true;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 10:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f411xe>::spi<3>::signal_mapper<io::spi::pin_signal::nss>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 4:
				case 15:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F411XE_SPI3_H
