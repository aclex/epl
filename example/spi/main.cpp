/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <string>

#include <epl/thread.h>
#include <epl/spi.h>

using namespace std;

using namespace epl;
using namespace epl::spi;

namespace
{
	using miso = gpio::pin<gpio::port::a, 6>;
	using mosi = gpio::pin<gpio::port::a, 7>;
	using sck = gpio::pin<gpio::port::a, 5>;
	using nss = gpio::pin<gpio::port::a, 4>;

	using spi0 = epl::spi::port<0, sck, miso, mosi, nss>;
}

int main(int, char**)
{
	spi0::configure(spi::mode::master, duplex::full, spi::word_length::b8, spi::prescaler::p8, spi::bit_order::msb, spi::nss_mode::pulse);

	spi0::enable();

	while(true)
	{
		spi0::write(0xde);
		this_thread::sleep_for(1s);
	}

	spi0::disable();
}
