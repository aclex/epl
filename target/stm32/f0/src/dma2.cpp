/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/mcu/stm32f0xx.h"

using namespace std;

using namespace stm32::epl;

void stm32::epl::DMA1_Channel2_3_IRQHandler() noexcept
{
	typedef peripherals<stm32::epl::mcu_model::stm32f0xx>::dma<2> device2;
	typedef peripherals<stm32::epl::mcu_model::stm32f0xx>::dma<3> device3;
	static constexpr const IRQn_Type request { DMA1_Channel2_3_IRQn };

	NVIC_DisableIRQ(request);
	device2::handle_irq();
	device3::handle_irq();
	NVIC_EnableIRQ(request);
}
