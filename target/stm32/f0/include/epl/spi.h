/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_SPI_H
#define STM32_CPP_EPL_MCU_STM32F0XX_SPI_H

#include <epl/state.h>
#include <epl/endian.h>

#include <epl/constexpr_map.h>

#include <epl/io/spi/pin_signal.h>
#include <epl/io/spi/mode.h>
#include <epl/io/spi/transfer_mode.h>
#include <epl/io/spi/clock.h>
#include <epl/io/spi/frame_mode.h>
#include <epl/io/spi/nss_mode.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t port_no> struct peripherals<mcu_model::stm32f0xx>::spi
		{
		private:
			struct constexpr_spec_init_helper
			{
				static constexpr SPI_TypeDef* handle() noexcept;
				static constexpr gpio::af af() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static constexpr std::uint32_t clock_flag() noexcept;
				static constexpr std::uint32_t reset_command() noexcept;
			};

		public:
			static constexpr SPI_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const gpio::af af = constexpr_spec_init_helper::af();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();

			template<io::spi::pin_signal> struct signal_mapper
			{
				template<gpio::port_name pn, std::size_t pin_no>
				static constexpr bool check() noexcept
				{
					constexpr bool result(find_mapping(pn, pin_no));
					static_assert(result,
						"Unable to connect the specified signal to the requested pin.");
					return result;
				}

				static constexpr bool find_mapping(gpio::port_name pn, std::size_t pin_no) noexcept
				{
					return false;
				}
			};

			static constexpr bool supports_data_packing() noexcept
			{
				return false;
			}

			template<state s> static constexpr void set_nss() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_SSI);

				set_bits(reg, mask, nss_state_value<s>());
			}

			template<state s> static constexpr void set_crc_next() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_CRCNEXT);

				set_bits(reg, mask, crc_next_value<s>());
			}

			static constexpr void wait_for_ready_to_receive() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->SR);
				while (!(reg & SPI_SR_RXNE));
			}

			static constexpr void wait_for_ready_to_send() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->SR);
				while (!(reg & SPI_SR_TXE));
			}

			static constexpr void wait_for_transfer_complete() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->SR);
				while (reg & SPI_SR_BSY);
			}

			static constexpr std::uint16_t data() noexcept
			{
				return handle->DR;
			}

			static constexpr void set_data(const std::uint16_t v) noexcept
			{
				handle->DR = v;
			}

			template<std::size_t threshold_length> static constexpr void set_threshold() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR2);
				constexpr const std::uint16_t mask(SPI_CR2_FRXTH);

				set_bits(reg, mask, rx_threshold_value<threshold_length>());
			}

			template<state s> static constexpr void set_enabled() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_SPE);

				set_bits(reg, mask, enable_state_value<s>());
			}

			static bool enabled() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_SPE);

				return reg & mask;
			}

			static constexpr void enable() noexcept
			{
				set_enabled<state::on>();
			}

			static constexpr void disable() noexcept
			{
				set_enabled<state::off>();
			}

			static constexpr void reset() noexcept
			{
				handle->CR1 = 0;
				handle->CR2 = 0;
			}

			template<epl::io::spi::mode m> static constexpr void set_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_MSTR);

				set_bits(reg, mask, mode_value<m>());
			}

			template<epl::io::spi::transfer_mode tm> static constexpr void set_transfer_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | SPI_CR1_RXONLY);

				set_bits(reg, mask, transfer_mode_value<tm>());
			}

			template<std::size_t baud_rate_divider> static constexpr void set_baud_rate_divider() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_BR);

				set_bits(reg, mask, baud_rate_control_value<baud_rate_divider>());
			}

			template<std::size_t data_length> static constexpr void set_data_length() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR2);
				constexpr const std::uint16_t mask(SPI_CR2_DS);

				set_bits(reg, mask, data_length_value<data_length>());
			}

			template<epl::io::spi::clock_polarity cpol> static constexpr void set_clock_polarity() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_CPOL);

				set_bits(reg, mask, clock_polarity_value<cpol>());
			}

			template<epl::io::spi::clock_phase cpha> static constexpr void set_clock_phase() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_CPHA);

				set_bits(reg, mask, clock_phase_value<cpha>());
			}

			template<endian endianness> static constexpr void set_shift_order() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_LSBFIRST);

				set_bits(reg, mask, shift_order_value<endianness>());
			}

			template<state crc> static constexpr void set_crc_state() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_CRCEN);

				set_bits(reg, mask, crc_state_value<crc>());
			}

			template<std::size_t crc_length> static constexpr void set_crc_length() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CR1);
				constexpr const std::uint16_t mask(SPI_CR1_CRCL);

				set_bits(reg, mask, crc_length_value<crc_length>());
			}

			template<epl::io::spi::nss_mode ssm> static constexpr void set_nss_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg1(handle->CR1);
				constexpr const std::uint16_t mask1(SPI_CR1_SSM);

				set_bits(reg1, mask1, ssm_value<ssm>());

				constexpr volatile std::uint16_t& reg2(handle->CR2);
				constexpr const std::uint16_t mask2(SPI_CR2_NSSP | SPI_CR2_SSOE);

				set_bits(reg2, mask2, nssp_value<ssm>());
			}

		private:
			template<state s> static constexpr std::uint16_t nss_state_value() noexcept
			{
				constexpr const std::size_t shift_amount(8);

				return static_cast<std::uint16_t>(s) << shift_amount;
			}

			template<state s> static constexpr std::uint16_t crc_next_value() noexcept
			{
				constexpr const std::size_t shift_amount(12);

				return static_cast<std::uint16_t>(s) << shift_amount;
			}

			template<state s> static constexpr std::uint16_t enable_state_value() noexcept
			{
				constexpr const std::size_t shift_amount(6);

				return static_cast<std::uint16_t>(s) << shift_amount;
			}

			template<epl::io::spi::mode m> static constexpr std::uint16_t mode_value() noexcept
			{
				constexpr const std::size_t shift_amount(2);

				return static_cast<std::uint16_t>(m) << shift_amount;
			}

			template<epl::io::spi::transfer_mode tm> static constexpr std::uint16_t transfer_mode_value() noexcept
			{
				switch (tm)
				{
				case epl::io::spi::transfer_mode::full_duplex:
					return 0;

				case epl::io::spi::transfer_mode::half_duplex:
					return SPI_CR1_BIDIMODE;

				case epl::io::spi::transfer_mode::rx_only:
					return SPI_CR1_RXONLY;

				case epl::io::spi::transfer_mode::tx_only:
					return SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE;
				}
			}

			template<std::size_t baud_rate_divider> static constexpr std::uint16_t baud_rate_control_value() noexcept
			{
				constexpr const std::size_t shift_amount(3);

				constexpr const epl::constexpr_map<std::size_t, std::uint16_t, 8> divider_map(
				{{
					{ 2, 0 },
					{ 4, 0b001 },
					{ 8, 0b010 },
					{ 16, 0b011 },
					{ 32, 0b100 },
					{ 64, 0b101 },
					{ 128, 0b110 },
					{ 256, 0b111 }
				}});

				static_assert(divider_map.count(baud_rate_divider), "Unsupported baud rate divider: should be power of 2 in range [2, 256].");

				return divider_map.at(baud_rate_divider) << shift_amount;
			}

			template<std::size_t data_length> static constexpr std::uint16_t data_length_value() noexcept
			{
				constexpr const std::size_t shift_amount(8);

				constexpr const epl::constexpr_map<std::size_t, std::uint16_t, 13> length_map(
				{{
					{ 4, 0b0011 },
					{ 5, 0b0100 },
					{ 6, 0b0101 },
					{ 7, 0b0110 },
					{ 8, 0b0111 },
					{ 9, 0b1000 },
					{ 10, 0b1001 },
					{ 11, 0b1010 },
					{ 12, 0b1011 },
					{ 13, 0b1100 },
					{ 14, 0b1101 },
					{ 15, 0b1110 },
					{ 16, 0b1111 }
				}});

				static_assert(length_map.count(data_length), "Unsupported data length: should be between 4 and 16 bits.");

				return length_map.at(data_length) << shift_amount;
			}

			template<io::spi::clock_polarity cpol> static constexpr std::uint16_t clock_polarity_value() noexcept
			{
				constexpr const std::size_t shift_amount(1);

				return static_cast<std::uint16_t>(cpol) << shift_amount;
			}

			template<io::spi::clock_phase cpha> static constexpr std::uint16_t clock_phase_value() noexcept
			{
				return static_cast<std::uint16_t>(cpha);
			}

			template<endian endianness> static constexpr std::uint16_t shift_order_value() noexcept
			{
				constexpr const std::size_t shift_amount(7);

				switch (endianness)
				{
				case endian::big:
					return 0;

				case endian::little:
					return 1 << shift_amount;
				}
			}

			template<state crc> static constexpr std::uint16_t crc_state_value() noexcept
			{
				constexpr const std::size_t shift_amount(13);

				return static_cast<std::uint16_t>(crc) << shift_amount;
			}

			template<std::size_t crc_length> static constexpr std::uint16_t crc_length_value() noexcept
			{
				constexpr const std::size_t shift_amount(11);

				constexpr const epl::constexpr_map<std::size_t, std::uint16_t, 2> length_map(
				{{
					{ 8, 0 },
					{ 16, 1 }
				}});

				static_assert(length_map.count(crc_length), "Unsupported CRC length: should be either 8 or 16 bits.");

				return length_map.at(crc_length) << shift_amount;
			}

			template<io::spi::nss_mode ssm> static constexpr std::uint16_t ssm_value() noexcept
			{
				switch (ssm)
				{
				case io::spi::nss_mode::software:
					return SPI_CR1_SSM;

				case io::spi::nss_mode::normal:
				case io::spi::nss_mode::pulse:
					return 0;
				}
			}

			template<epl::io::spi::nss_mode ssm> static constexpr std::uint16_t nssp_value() noexcept
			{
				switch (ssm)
				{
				case io::spi::nss_mode::software:
					return 0;

				case io::spi::nss_mode::normal:
					return SPI_CR2_SSOE;

				case io::spi::nss_mode::pulse:
					return SPI_CR2_NSSP | SPI_CR2_SSOE;
				}
			}

			template<std::size_t threshold_length> static constexpr std::uint16_t rx_threshold_value() noexcept
			{
				constexpr const std::size_t shift_amount(12);

				constexpr const epl::constexpr_map<std::size_t, std::uint16_t, 2> length_map(
				{{
					{ 16, 0 },
					{ 8, 1 }
				}});

				static_assert(length_map.count(threshold_length), "Unsupported threshold value: should be either 8 (1/4 of register length) or 16 (1/2 of register length) bits.");

				return length_map.at(threshold_length) << shift_amount;
			}
		};
	}
}

#include "spi1.h"
#include "spi2.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_SPI_H
