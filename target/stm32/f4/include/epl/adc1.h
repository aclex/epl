/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_ADC1_H
#define STM32_CPP_EPL_MCU_STM32F0XX_ADC1_H

#include <epl/adc/internal_channels.h>

extern ADC_TypeDef hw_adc_1;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr ADC_TypeDef* peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_adc_1;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb2;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB2ENR_ADC1EN;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::reset_command() noexcept
		{
			return RCC_APB2RSTR_ADC1RST;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::adc<1>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::adc<1>::channel_mapper::get_channel(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 0:
					return 0;

				case 1:
					return 1;

				case 2:
					return 2;

				case 3:
					return 3;

				case 4:
					return 4;

				case 5:
					return 5;

				case 6:
					return 6;

				case 7:
					return 7;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 0:
					return 8;

				case 1:
					return 9;
				}

			case gpio::PC:
				switch (pin_no)
				{
				case 0:
					return 10;

				case 1:
					return 11;

				case 2:
					return 12;

				case 3:
					return 13;

				case 4:
					return 14;

				case 5:
					return 15;
				}
			}
		}

		template<> template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::adc<1>::channel_mapper::get_channel<epl::adc::temp_sensor>() noexcept
		{
			return 16;
		}

		template<> template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::adc<1>::channel_mapper::get_channel<epl::adc::vref>() noexcept
		{
			return 17;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_ADC1_H
