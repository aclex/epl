/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_RCU_H
#define GD32VF103XX_EPL_RCU_H

#include <chrono>

#include <epl/mcu/bus.h>
#include <epl/mcu/register_map.h>

namespace epl::rcu
{
	template<mcu::bus b, std::uint32_t flag_mask> static constexpr void enable_clock() noexcept
	{
		const auto pv{*mcu::register_map::rcu::enable<b>};
		*mcu::register_map::rcu::enable<b> = pv | flag_mask;
	}

	template<mcu::bus b, std::uint32_t command_mask> static constexpr void reset_sequence() noexcept
	{
		const auto pv{*mcu::register_map::rcu::enable<b>};
		*mcu::register_map::rcu::reset<b> = pv | command_mask;
		*mcu::register_map::rcu::reset<b> = pv & ~command_mask;
	}

	template<typename Rep, typename Period>
	static constexpr std::uint64_t to_ticks(const std::chrono::duration<Rep, Period>& d) noexcept
	{
		using namespace std::chrono;

		constexpr auto ticks_per_us{2};

		return duration_cast<microseconds>(d).count() * ticks_per_us;
	}
}

#endif // GD32VF103XX_EPL_RCU_H
