/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <string>

#include <epl/chrono.h>

#include <epl/io/usart.h>

using namespace std;

using namespace stm32::epl;

namespace
{
	typedef stm32::epl::chrono::steady_clock mcu_clock;

	typedef gpio::pin<mcu::gpio::PA, 3> rx_pin;
	typedef gpio::pin<mcu::gpio::PA, 2> tx_pin;

	template<std::size_t no> using usart = io::usart::usart<no, rx_pin, tx_pin>;

	bool fire { false };
}

int main(void)
{
	mcu_clock::start();

	usart<2>::configure<115200, 8, 1, io::usart::parity::no>();

	usart<2>::enable();

	usart<2>::set_interrupt_handler<io::usart::interrupt::rxne>(
		[]()
		{
			if (!fire)
			{
				unsigned char c { };
				usart<2>() >> c;
				usart<2>() << c << "\r\n";
				fire = true;
			}
		});

	const string msg("Hello, world!\r\n");

	while(true)
	{
		usart<2>() << msg;

		mcu_clock::sleep_for(1s);
		fire = false;
	}
}
