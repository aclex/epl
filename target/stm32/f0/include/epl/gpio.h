/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_GPIO_H
#define STM32_CPP_EPL_MCU_STM32F0XX_GPIO_H

#include <epl/util.h>

#include <epl/gpio/mode.h>
#include <epl/gpio/push_pull.h>
#include <epl/gpio/output_mode.h>

extern GPIO_TypeDef hw_gpio_a;
extern GPIO_TypeDef hw_gpio_b;
extern GPIO_TypeDef hw_gpio_c;

namespace stm32
{
	namespace epl
	{
		template<> struct peripherals<mcu_model::stm32f0xx>::gpio
		{
			enum port_name : unsigned char
			{
				PA,
				PB,
				PC,
				PD,
				PF
			};

			template<port_name pn> struct port;

			enum class speed : unsigned char
			{
				low = 0x0, // 2 MHz
				medium = 0x1, // 10 MHz
				high = 0x3 // 50 MHz
			};

			enum class af : unsigned char
			{
				gpio,
				tim1,
				tim3,
				tim14,
				tim15,
				tim16,
				tim17,
				i2c1,
				i2c2,
				spi1,
				spi2,
				usart1,
				usart2,
				usart3,
				usart4,
				usart5,
				usart6,
				eventout,
				mco,
				swdio,
				swclk,
				ir_out
			};

			template<af f> struct af_mapper
			{
				static constexpr const std::uint8_t s_not_found_value = 0xff;
				template<port_name pn, std::size_t pin_no>
				static constexpr std::uint8_t value()
				{
					constexpr std::uint8_t result(find_mapping(pn, pin_no));
					static_assert(result != s_not_found_value,
						"No requested alternate function found for the specified pin.");
					return result;
				}

				static constexpr std::uint8_t find_mapping(port_name pn, std::size_t pin_no)
				{
					return s_not_found_value;
				}
			};

			template<mode m> constexpr std::uint32_t mode_value() noexcept
			{
				switch (m)
				{
				case mode::input:
					return 0b00;

				case mode::output:
					return 0b01;

				case mode::af:
					return 0b10;

				case mode::analog:
					return 0b11;
				}
			}

			template<output_mode ot> constexpr std::uint32_t output_mode_value() noexcept
			{
				switch (ot)
				{
				case output_mode::push_pull:
					return 0b00;

				case output_mode::open_drain:
					return 0b01;
				}
			}

			template<pull pl> constexpr std::uint32_t pull_value() noexcept
			{
				switch (pl)
				{
				default:
				case pull::none:
					return 0b00;

				case pull::up:
					return 0b01;

				case pull::down:
					return 0b10;
				}
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, mode m>
			void set_mode() noexcept
			{
				constexpr std::size_t shift_amount(pin_no * 2);
				constexpr std::uint32_t mask { 0b11u << shift_amount };

				set_bits(mcu::gpio::port<p>::handle->MODER, mask, mode_value<m>() << shift_amount);
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, output_mode ot>
			void set_output_mode() noexcept
			{
				constexpr std::size_t shift_amount(pin_no);
				constexpr std::uint32_t mask { 0b1u << shift_amount };

				set_bits(mcu::gpio::port<p>::handle->OTYPER, mask, output_mode_value<ot>() << shift_amount);
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, mcu::gpio::speed sp>
			constexpr void set_speed() noexcept
			{
				constexpr std::size_t shift_amount(pin_no * 2);
				constexpr std::uint32_t mask { 0b11u << shift_amount };

				set_bits(mcu::gpio::port<p>::handle->OSPEEDR, mask, static_cast<std::uint32_t>(sp) << shift_amount);
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, pull pl>
			constexpr void set_pull() noexcept
			{
				constexpr std::size_t shift_amount(pin_no * 2);
				constexpr std::uint32_t mask { 0b11u << shift_amount };

				set_bits(mcu::gpio::port<p>::handle->PUPDR, mask, pull_value<pl>() << shift_amount);
			}

			template<mcu::gpio::port_name p, std::size_t pin_no> constexpr volatile std::uint32_t& af_register() noexcept
			{
				if constexpr(pin_no < 8)
					return mcu::gpio::port<p>::handle->AFR[0];
				else
					return mcu::gpio::port<p>::handle->AFR[1];
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, std::uint8_t af_value>
			constexpr void set_af() noexcept
			{
				constexpr std::size_t shift_amount(pin_no < 8 ? pin_no * 4 : (pin_no - 8) * 4);
				constexpr std::uint32_t mask { 0b1111u << shift_amount };

				set_bits(af_register<p, pin_no>(), mask, af_value << shift_amount);
			}

			template<mcu::gpio::port_name p, std::size_t pin_no, mcu::gpio::af f>
			constexpr void set_af() noexcept
			{
				set_af<p, pin_no, mcu::gpio::af_mapper<f>::template value<p, pin_no>()>();
			}

			template<mcu::gpio::port_name p, std::size_t no> struct pin
			{
				static constexpr mcu::gpio::port_name port_name = p;
				typedef mcu::gpio::port<port_name> port;
				static constexpr std::size_t pin_no = no;

				template
				<
					mode m,
					pull pl = pull::none,
					mcu::gpio::speed sp = mcu::gpio::speed { }
				>
				static void configure() noexcept
				{
					if constexpr(m != mode::analog)
					{
						port::clock_bus::template enable_clock<port::clock_flag>();
					}

					set_mode<p, pin_no, m>();
					set_speed<p, pin_no, sp>();
					set_pull<p, pin_no, pl>();
				}

				template
				<
					output_mode ot,
					pull pl = pull::none,
					mcu::gpio::speed sp = mcu::gpio::speed { }
				>
				static void configure() noexcept
				{
					pin::configure<mode::output, pl, sp>();
					set_output_mode<p, pin_no, ot>();
				}

				template
				<
					mcu::gpio::af af,
					output_mode ot = output_mode::push_pull,
					pull pl = pull::none,
					mcu::gpio::speed sp = mcu::gpio::speed { }
				>
				static void configure() noexcept
				{
					pin::configure<mode::af, pl, sp>();

					set_output_mode<p, pin_no, ot>();

					if constexpr(af != mcu::gpio::af::gpio)
					{
						set_af<port_name, pin_no, af>();
					}
				}

				static void unbind() noexcept
				{
					pin::configure<mode::input>();
				}

				static void toggle() noexcept
				{
					port::handle->ODR ^= (1 << pin_no);
				}

				template<state s> static constexpr void set() noexcept
				{
					if constexpr (s == state::off)
					{
						port::handle->ODR &= ~(1 << pin_no);
					}
					else
					{
						port::handle->ODR |= (1 << pin_no);
					}
				}

				static constexpr state value() noexcept
				{
					return state(port::handle->ODR & (1 << pin_no));
				}
			};
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::gpio::port<peripherals<mcu_model::stm32f0xx>::gpio::PA>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_a;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHBENR_GPIOAEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::gpio::port<peripherals<mcu_model::stm32f0xx>::gpio::PB>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_b;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHBENR_GPIOBEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::gpio::port<peripherals<mcu_model::stm32f0xx>::gpio::PC>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_c;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHBENR_GPIOCEN;
		};

		// GPIO alternative functions

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
				case PA:
					switch (pin_no)
					{
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
						return 2;
					}

				case PB:
					switch (pin_no)
					{
					case 0:
					case 1:
					case 12:
					case 13:
					case 14:
					case 15:
						return 2;
					}

			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim3>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 6:
				case 7:
					return 1;
				}

			case PB:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 4:
				case 5:
					return 1;
				}

			case PC:
				switch (pin_no)
				{
				case 6:
				case 7:
				case 8:
				case 9:
					return 0;
				}

			case PD:
				switch (pin_no)
				{
				case 2:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim14>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 4:
				case 7:
					return 4;
				}

			case PB:
				switch (pin_no)
				{
				case 1:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim15>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 1:
					return 5;

				case 2:
				case 3:
				case 9:
					return 0;
				}

			case PB:
				switch (pin_no)
				{
				case 12:
					return 5;
				case 14:
					return 1;

				case 15:
					return 1; // also available AF 3 depending on function
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim16>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 6:
					return 5;
				}

			case PB:
				switch (pin_no)
				{
				case 5:
				case 6:
				case 8:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::tim17>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 7:
					return 5;
				case 10:
					return 0;
				}

			case PB:
				switch (pin_no)
				{
				case 4:
					return 5;
				case 7:
				case 9:
					return 2;
				case 8:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::i2c1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 9:
				case 10:
					return 4;
				}

			case PB:
				switch (pin_no)
				{
				case 5:
					return 3;
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
					return 1;
				}

			case PF:
				switch (pin_no)
				{
				case 0:
				case 1:
					return 1;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::i2c2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 11:
				case 12:
					return 5;
				}

			case PB:
				switch (pin_no)
				{
				case 10:
				case 11:
					return 1;

				case 13:
				case 14:
					return 5;
				}

			case PF:
				switch (pin_no)
				{
				case 0:
				case 1:
					return 1;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::spi1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 4:
				case 5:
				case 6:
				case 7:
				case 15:
					return 0;
				}

			case PB:
				switch (pin_no)
				{
				case 3:
				case 4:
				case 5:
				case 12:
				case 13:
				case 14:
				case 15:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::spi2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 9:
				case 10:
					return 5;

				case 12:
				case 13:
				case 14:
				case 15:
					return 0;
				}

			case PC:
				switch (pin_no)
				{
				case 2:
				case 3:
					return 1;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 8:
				case 9:
				case 10:
				case 11:
				case 14:
				case 15:
					return 1;
				}

			case PB:
				switch (pin_no)
				{
				case 6:
				case 7:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 14:
				case 15:
					return 1;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart3>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 6:
					return 4;
				}

			case PB:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
					return 4;
				}

			case PC:
				switch (pin_no)
				{
				case 4:
				case 5:
				case 10:
				case 11:
				case 12:
					return 1;
				}

			case PD:
				switch (pin_no)
				{
				case 2:
					return 1;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart4>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 15:
					return 4;
				}

			case PB:
				switch (pin_no)
				{
				case 7:
					return 4;
				}

			case PC:
				switch (pin_no)
				{
				case 10:
				case 11:
				case 12:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart5>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 3:
				case 4:
				case 5:
					return 4;
				}

			case PC:
				switch (pin_no)
				{
				case 12:
					return 2;
				}

			case PD:
				switch (pin_no)
				{
				case 2:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::usart6>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 4:
				case 5:
					return 5;
				}

			case PC:
				switch (pin_no)
				{
				case 0:
				case 1:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::eventout>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 1:
				case 11:
				case 12:
					return 0;
				case 6:
				case 7:
					return 6;

				case 8:
				case 15:
					return 3;

				}

			case PB:
				switch (pin_no)
				{
				case 0:
				case 11:
					return 0;

				case 3:
				case 12:
					return 1;

				case 4:
					return 2;

				case 9:
					return 3;
				}

			case PC:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::mco>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 8:
					return 0;

				case 9:
					return 5;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::swdio>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 13:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::swclk>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 14:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f0xx>::gpio::af_mapper<peripherals<mcu_model::stm32f0xx>::gpio::af::ir_out>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 13:
					return 1;
				}

			case PB:
				switch (pin_no)
				{
				case 9:
					return 0;
				}
			}

			return s_not_found_value;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_GPIO_H
