/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_MCU_REGISTER_MAP_H
#define GD32VF103XX_EPL_MCU_REGISTER_MAP_H

#include <cstdint>

#include <epl/mcu/bus.h>
#include <epl/gpio/port.h>

extern std::uint32_t reg_ahb1_bus_base;
extern std::uint32_t reg_apb1_bus_base;
extern std::uint32_t reg_apb2_bus_base;

extern std::uint64_t reg_timer_ctrl;

extern std::uint8_t reg_eclic_base;
extern std::uint32_t reg_eclic_info;

namespace epl::mcu::register_map
{
	template<bus d> constexpr volatile std::uint32_t* bus_base{};
	template<> constexpr volatile std::uint32_t* bus_base<bus::ahb>{&reg_ahb1_bus_base};
	template<> constexpr volatile std::uint32_t* bus_base<bus::apb1>{&reg_apb1_bus_base};
	template<> constexpr volatile std::uint32_t* bus_base<bus::apb2>{&reg_apb2_bus_base};
}

namespace epl::mcu::register_map::rcu
{
	constexpr volatile std::uint32_t* base{register_map::bus_base<bus::ahb> + 0x2400};

	constexpr volatile std::uint32_t* ctl{base};
	constexpr volatile std::uint32_t* cfg0{base + 1};
	constexpr volatile std::uint32_t* cfg1{base + 11};
	constexpr volatile std::uint32_t* intr{base + 2};
	constexpr volatile std::uint32_t* bdctl{base + 8};
	constexpr volatile std::uint32_t* rstsck{base + 9};
	constexpr volatile std::uint32_t* dsv{base + 13};

	template<bus d> constexpr volatile std::uint32_t* enable{base};
	template<> constexpr volatile std::uint32_t* enable<bus::apb1>{base + 7};
	template<> constexpr volatile std::uint32_t* enable<bus::apb2>{base + 6};
	template<> constexpr volatile std::uint32_t* enable<bus::ahb>{base + 5};

	template<bus d> constexpr volatile std::uint32_t* reset{base};
	template<> constexpr volatile std::uint32_t* reset<bus::ahb>{base + 10};
	template<> constexpr volatile std::uint32_t* reset<bus::apb1>{base + 4};
	template<> constexpr volatile std::uint32_t* reset<bus::apb2>{base + 3};
}

namespace epl::mcu::register_map::timer_unit
{
	constexpr volatile std::uint64_t* ctrl{&reg_timer_ctrl};

	constexpr volatile std::uint64_t* mtime{ctrl};
	constexpr volatile std::uint64_t* mtimecmp{ctrl + 1};
}

namespace epl::mcu::register_map::gpio
{
	constexpr volatile std::uint32_t* base{bus_base<bus::apb2> + 0x0200};

	template<epl::gpio::port p> constexpr volatile std::uint32_t* port_base{base};

	template<> constexpr volatile std::uint32_t* port_base<epl::gpio::port::a>{base};
	template<> constexpr volatile std::uint32_t* port_base<epl::gpio::port::b>{base + 0x0100};
	template<> constexpr volatile std::uint32_t* port_base<epl::gpio::port::c>{base + 0x0200};
	template<> constexpr volatile std::uint32_t* port_base<epl::gpio::port::d>{base + 0x0300};
	template<> constexpr volatile std::uint32_t* port_base<epl::gpio::port::e>{base + 0x0400};

	template<epl::gpio::port p> constexpr bus port_bus{bus::apb2};

	template<epl::gpio::port p> constexpr volatile std::uint32_t* ctl0{port_base<p>};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* ctl1{port_base<p> + 1};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* istat{port_base<p> + 2};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* octl{port_base<p> + 3};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* bop{port_base<p> + 4};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* bc{port_base<p> + 5};
	template<epl::gpio::port p> constexpr volatile std::uint32_t* lock{port_base<p> + 6};
}

namespace epl::mcu::register_map::afio
{
	constexpr volatile std::uint32_t* base{bus_base<bus::apb2>};

	constexpr volatile std::uint32_t* ec{base};
	constexpr volatile std::uint32_t* pcf0{base + 1};
	constexpr volatile std::uint32_t* pcf1{base + 7};
	constexpr volatile std::uint32_t* extiss0{base + 2};
	constexpr volatile std::uint32_t* extiss1{base + 3};
	constexpr volatile std::uint32_t* extiss2{base + 4};
	constexpr volatile std::uint32_t* extiss3{base + 5};

	template<std::size_t number> constexpr volatile std::uint32_t* extiss{extiss0 + number / 4};
}

namespace epl::mcu::register_map::exti
{
	constexpr volatile std::uint32_t* base{bus_base<bus::apb2> + 0x0400 / 4};

	constexpr volatile std::uint32_t* inten{base};
	constexpr volatile std::uint32_t* even{base + 1};
	constexpr volatile std::uint32_t* rten{base + 2};
	constexpr volatile std::uint32_t* ften{base + 3};
	constexpr volatile std::uint32_t* swiev{base + 4};
	constexpr volatile std::uint32_t* pd{base + 5};
}

namespace epl::mcu::register_map::spi
{
	template<std::size_t n> constexpr volatile std::uint32_t* if_base;

	template<> constexpr volatile std::uint32_t* if_base<0>{bus_base<bus::apb2> + 0x00003000 / 4};
	template<> constexpr volatile std::uint32_t* if_base<1>{bus_base<bus::apb1> + 0x00003800 / 4};
	template<> constexpr volatile std::uint32_t* if_base<2>{bus_base<bus::apb1> + 0x00003c00 / 4};

	template<std::size_t n> constexpr bus if_bus{bus::apb1};
	template<> constexpr bus if_bus<0>{bus::apb2};

	template<std::size_t n> constexpr volatile std::uint32_t* ctl0{if_base<n>};
	template<std::size_t n> constexpr volatile std::uint32_t* ctl1{if_base<n> + 0x04 / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* stat{if_base<n> + 0x08 / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* data{if_base<n> + 0x0c / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* crcpoly{if_base<n> + 0x10 / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* rcrc{if_base<n> + 0x14 / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* tcrc{if_base<n> + 0x18 / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* i2sctl{if_base<n> + 0x1c / 4};
	template<std::size_t n> constexpr volatile std::uint32_t* i2spsc{if_base<n> + 0x20 / 4};
}

namespace epl::mcu::register_map::dma
{
	template<std::size_t n> constexpr bus part_bus{bus::ahb};

	template<std::size_t n> constexpr volatile std::uint32_t* part_base;

	template<> constexpr volatile std::uint32_t* part_base<0>{bus_base<bus::ahb> + 0x00008000 / 4};
	template<> constexpr volatile std::uint32_t* part_base<1>{bus_base<bus::ahb> + 0x00008400 / 4};

	template<std::size_t n> constexpr volatile std::uint32_t* intf{part_base<n>};
	template<std::size_t n> constexpr volatile std::uint32_t* intc{part_base<n> + 0x04 / 4};
	template<std::size_t n, std::size_t ch> constexpr volatile std::uint32_t* ctl{part_base<n> + (0x08 + 0x14 * ch) / 4};
	template<std::size_t n, std::size_t ch> constexpr volatile std::uint32_t* cnt{part_base<n> + (0x0c + 0x14 * ch) / 4};
	template<std::size_t n, std::size_t ch> constexpr volatile std::uint32_t* paddr{part_base<n> + (0x10 + 0x14 * ch) / 4};
	template<std::size_t n, std::size_t ch> constexpr volatile std::uint32_t* maddr{part_base<n> + (0x14 + 0x14 * ch) / 4};
}

namespace epl::mcu::register_map::eclic
{
	constexpr volatile std::uint8_t* base{&reg_eclic_base};

	constexpr volatile std::uint8_t* cfg{base};
	constexpr const std::uint32_t* info{&reg_eclic_info};
	constexpr volatile std::uint8_t* mth{base + 0x0b};

	constexpr volatile std::uint8_t* int_ip(const std::size_t n) {return base + 0x1000 + 4 * n;}
	constexpr volatile std::uint8_t* int_ie(const std::size_t n) {return base + 0x1001 + 4 * n;}
	constexpr volatile std::uint8_t* int_attr(const std::size_t n) {return base + 0x1002 + 4 * n;}
	constexpr volatile std::uint8_t* int_ctl(const std::size_t n) {return base + 0x1003 + 4 * n;}
}

namespace epl::mcu::register_map::csr
{
	constexpr std::uint32_t mstatus{0x300};
	constexpr std::uint32_t mtvec{0x305};
	constexpr std::uint32_t mtvt{0x307};
	constexpr std::uint32_t mepc{0x341};
	constexpr std::uint32_t mcause{0x342};
	constexpr std::uint32_t mnxti{0x345};
	constexpr std::uint32_t mnvec{0x7c3};
	constexpr std::uint32_t msubm{0x7c4};
	constexpr std::uint32_t mmisc_ctl{0x7d0};
	constexpr std::uint32_t mtvt2{0x7ec};
	constexpr std::uint32_t minstret{0xb02};
	constexpr std::uint32_t wfe{0x810};

	constexpr std::uint32_t jalmnxti{0x7ed};
}

#endif // GD32VF103XX_EPL_MCU_REGISTER_MAP_H
