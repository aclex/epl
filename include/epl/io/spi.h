/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_IO_SPI_H
#define STM32_CPP_EPL_IO_SPI_H

#include <epl/mcu.h>
#include <epl/gpio.h>

#include <epl/io/spi/pin_signal.h>
#include <epl/io/spi/mode.h>
#include <epl/io/spi/transfer_mode.h>
#include <epl/io/spi/clock.h>
#include <epl/io/spi/frame_mode.h>
#include <epl/io/spi/nss_mode.h>

namespace stm32
{
	namespace epl
	{
		namespace io
		{
			namespace spi
			{
				template
				<
					std::size_t no,
					typename MISOPin,
					typename MOSIPin,
					typename SCKPin,
					typename NSSPin
				>
				class spi
				{
				private:
					typedef mcu::spi<no> port;

					typedef MISOPin miso_pin;
					typedef MOSIPin mosi_pin;
					typedef SCKPin sck_pin;
					typedef NSSPin nss_pin;

				public:
					template<mode m> static constexpr void configure() noexcept
					{
						port::clock_bus::template enable_clock<port::clock_flag>();

						spi::disable();
						port::reset();

						port::template set_mode<m>();

						configure_gpio();
					}

					static constexpr void enable() noexcept
					{
						port::template set_enabled<state::on>();
					}

					static constexpr void disable() noexcept
					{
						if (port::enabled())
						{
							port::wait_for_ready_to_send();
							port::wait_for_transfer_complete();
						}

						port::template set_enabled<state::off>();
					}

					static constexpr std::uint16_t read() noexcept
					{
						port::wait_for_ready_to_receive();
						return port::data();
					}

					static void write(std::uint16_t v) noexcept
					{
						port::wait_for_ready_to_send();
						port::set_data(v);
					}

					static void sync_write(std::uint16_t v) noexcept
					{
						write(v);
						port::wait_for_transfer_complete();
					}

					template
					<
						typename VectorType,
						typename std::enable_if<!std::is_integral<VectorType>::value, int>::type i = 0
					>
					static void write(const VectorType& v)
					{
						if constexpr (port::supports_data_packing() && sizeof(v[0]) == 1)
						{
							bool ready(true);
							std::uint16_t acc { };
							for (auto e : v)
							{
								if (ready)
								{
									ready = false;
									acc = e;
								}
								else
								{
									acc |= (static_cast<std::uint16_t>(e) << 8);
									ready = true;
									write(acc);
									acc = 0;
								}
							}

							if (!ready)
								write(acc);
						}
						else
						{
							for (auto e : v)
							{
								write(e);
							}
						}
					}

					template
					<
						typename VectorType,
						typename std::enable_if<!std::is_integral<VectorType>::value, int>::type i = 0
					>
					static void sync_write(const VectorType& v)
					{
						write(v);
						port::wait_for_transfer_complete();
					}

					template<transfer_mode tm> static void set_transfer_mode() noexcept
					{
						port::template set_transfer_mode<tm>();
					}

					template<std::size_t baud_rate_divider> static void set_baud_rate_divider() noexcept
					{
						port::template set_baud_rate_divider<baud_rate_divider>();
					}

					template<std::size_t data_length> static void set_data_length() noexcept
					{
						port::template set_data_length<data_length>();
					}

					template<clock_polarity cpol> static void set_clock_polarity() noexcept
					{
						port::template set_clock_polarity<cpol>();
					}

					template<clock_phase cph> static void set_clock_phase() noexcept
					{
						port::template set_clock_phase<cph>();
					}

					template<endian shift_order> static void set_shift_order() noexcept
					{
						port::template set_shift_order<shift_order>();
					}

					template<state st> static void set_crc_state() noexcept
					{
						port::template set_crc_state<st>();
					}

					template<std::size_t crc_length> static void set_crc_length() noexcept
					{
						port::template set_crc_length<crc_length>();
					}

					template<nss_mode ssm> static void set_nss_mode() noexcept
					{
						port::template set_nss_mode<ssm>();
					}

				private:
					static constexpr void configure_gpio() noexcept
					{
						if constexpr(!std::is_void<miso_pin>::value)
						{
							port::template signal_mapper<pin_signal::miso>::template check<miso_pin::port_name, miso_pin::pin_no>();
							miso_pin::template configure
							<
								port::af, gpio::output_type::push_pull,
								gpio::pull::none, mcu::gpio::speed::high
							>();
						}

						if constexpr(!std::is_void<mosi_pin>::value)
						{
							port::template signal_mapper<pin_signal::mosi>::template check<mosi_pin::port_name, mosi_pin::pin_no>();
							mosi_pin::template configure
							<
								port::af, gpio::output_type::push_pull,
								gpio::pull::none, mcu::gpio::speed::high
							>();
						}

						if constexpr(!std::is_void<sck_pin>::value)
						{
							port::template signal_mapper<pin_signal::sck>::template check<sck_pin::port_name, sck_pin::pin_no>();
							sck_pin::template configure
							<
								port::af, gpio::output_type::push_pull,
								gpio::pull::none, mcu::gpio::speed::high
							>();
						}

						if constexpr(!std::is_void<nss_pin>::value)
						{
							port::template signal_mapper<pin_signal::nss>::template check<nss_pin::port_name, nss_pin::pin_no>();
							nss_pin::template configure
							<
								port::af, gpio::output_type::push_pull,
								gpio::pull::up, mcu::gpio::speed::high
							>();
						}
					}

				};
			}
		}
	}
}

#endif // STM32_CPP_EPL_IO_SPI_H
