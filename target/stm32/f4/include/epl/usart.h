/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F411XE_USART_H
#define STM32_CPP_EPL_MCU_STM32F411XE_USART_H

#include <epl/io/usart/pin_signal.h>
#include <epl/io/usart/parity.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t port_no> struct peripherals<mcu_model::stm32f411xe>::usart<port_no>
		{
		private:
			struct constexpr_spec_init_helper
			{
				static constexpr USART_TypeDef* handle() noexcept;
				static constexpr gpio::af af() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static std::uint32_t clock_flag() noexcept;
			};

			static constexpr std::uint16_t s_9bit_mask = 0x01ff;

		public:
			static constexpr USART_TypeDef* const handle = constexpr_spec_init_helper::handle();
			static constexpr const gpio::af af = constexpr_spec_init_helper::af();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();

			template<io::usart::pin_signal> struct signal_mapper
			{
				template<gpio::port_name pn, std::size_t pin_no>
				static constexpr bool check() noexcept
				{
					constexpr bool result(find_mapping(pn, pin_no));
					static_assert(result,
						"Unable to connect the specified signal to the requested pin.");
					return result;
				}

				static constexpr bool find_mapping(gpio::port_name pn, std::size_t pin_no) noexcept
				{
					return false;
				}
			};

			static void enable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_UE);

				set_bits(reg, mask, USART_CR1_UE);
			}

			static void disable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_UE);

				set_bits(reg, mask, 0);
			}

			static std::uint16_t read() noexcept
			{
				return handle->DR & s_9bit_mask;
			}

			static void write(const std::uint16_t v) noexcept
			{
				handle->DR = v & s_9bit_mask;
			}

			template<std::size_t word_length> static constexpr void set_word_length() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_M);

				set_bits(reg, mask, word_length_value<word_length>());
			}

			template<epl::io::usart::parity par> static constexpr void set_parity() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_PCE | USART_CR1_PS);

				set_bits(reg, mask, parity_value<par>());
			}

			template<typename RXPin, typename TXPin> static constexpr void set_mode() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_RE | USART_CR1_TE);

				set_bits(reg, mask, mode_value<RXPin, TXPin>());
			}

			template<typename StopBitsRatio> static constexpr void set_stop_bits() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR2_STOP);

				set_bits(reg, mask, stop_bits_value<StopBitsRatio>());
			}

			template<typename RTSPin, typename CTSPin> static constexpr void set_flow_control() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR3_RTSE | USART_CR3_CTSE);

				set_bits(reg, mask, flow_control_value<RTSPin, CTSPin>());
			}

			template<std::size_t baud_rate> static constexpr void set_overdrive() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR1);
				constexpr const std::uint32_t mask(USART_CR1_OVER8);

				set_bits(reg, mask, overdrive_value<baud_rate>());
			}

			template<std::size_t baud_rate> static constexpr void set_speed() noexcept
			{
				handle->BRR = speed_value<baud_rate>();
			}

			static constexpr void wait_for_ready_to_receive() noexcept
			{
				while (!(handle->SR & USART_SR_RXNE));
			}

			static constexpr void wait_for_ready_to_send() noexcept
			{
				while (!(handle->SR & USART_SR_TXE));
			}

			static constexpr void wait_for_transfer_complete() noexcept
			{
				while (!(handle->SR & USART_SR_TC));
			}

			template<typename StopBitsRatio> static constexpr std::uint32_t stop_bits_value() noexcept
			{
				constexpr float stop_bits(StopBitsRatio::num / StopBitsRatio::den);

				static_assert(stop_bits == 1 || stop_bits == 2, "Only 1 or 2 stop bits is supported by STM32F411xe.");

				switch (StopBitsRatio::num)
				{
				default:
				case 1:
					return 0b00;

				case 2:
					return 0b10;
				}
			}

			template<std::size_t word_length> static constexpr std::uint32_t word_length_value() noexcept
			{
				static_assert(word_length == 8 || word_length == 9, "Word lengths of 8 and 9 bits are only supported.");

				switch (word_length)
				{
				default:
				case 8:
					return 0;

				case 9:
					return USART_CR1_M;
				}
			}

			template<epl::io::usart::parity par> static constexpr std::uint32_t parity_value() noexcept
			{
				switch (par)
				{
				default:
				case epl::io::usart::parity::no:
					return 0;

				case epl::io::usart::parity::even:
					return USART_CR1_PCE;

				case epl::io::usart::parity::odd:
					return USART_CR1_PCE | USART_CR1_PS;
				}
			}

			template<typename RXPin, typename TXPin> static constexpr std::uint32_t mode_value() noexcept
			{
				return USART_CR1_RE * (!std::is_void<RXPin>::value) | USART_CR1_TE * (!std::is_void<TXPin>::value);
			}

			template<typename RTSPin, typename CTSPin> static constexpr std::uint32_t flow_control_value() noexcept
			{
				return USART_CR3_RTSE * (!std::is_void<RTSPin>::value) | USART_CR3_CTSE * (!std::is_void<CTSPin>::value);
			}

			template<std::size_t baud_rate> static constexpr std::uint32_t overdrive_value() noexcept
			{
				if (baud_rate <= 3000000)
				{
					return 0;
				}
				else
				{
					return USART_CR1_OVER8;
				}
			}

			template<std::size_t baud_rate> static constexpr std::uint16_t speed_value() noexcept
			{
				constexpr bool low_oversample(overdrive_value<baud_rate>());

				if (!low_oversample)
				{
					return real_clock_frequency<baud_rate>();
				}
				else
				{
					const std::uint16_t result(real_clock_frequency<baud_rate>() * 2);
					return (result & ~0b111) | ((result & 0b111) >> 1);
				}
			}

			template<std::size_t baud_rate> static constexpr std::size_t real_clock_frequency() noexcept
			{
				static_assert(baud_rate > 0, "Baud rate shall be positive.");
				return clock_frequency() / baud_rate + ((clock_frequency() % baud_rate) > baud_rate / 2);
			}

			static constexpr bool supports_flow_control() noexcept
			{
				return port_no != 6;
			}

			static constexpr std::size_t clock_frequency() noexcept
			{
				return rcc::apb_clock_frequency();
			}
		};
	}
}

#include "usart1.h"
#include "usart2.h"
#include "usart6.h"

#endif // STM32_CPP_EPL_MCU_STM32F411XE_USART_H
