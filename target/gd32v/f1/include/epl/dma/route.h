/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_DMA_ROUTE_H
#define GD32VF103XX_EPL_DMA_ROUTE_H

#include <epl/dma/target_type.h>

namespace epl::dma
{
	template<target_type from, target_type to> struct route
	{
		static_assert(from == target_type::peripheral && to == target_type::peripheral, "Peripheral to peripheral DMA streams aren't supported for this MCU.");
	};

	template<> struct route<target_type::peripheral, target_type::memory> {};
	template<> struct route<target_type::memory, target_type::peripheral> {};
	template<> struct route<target_type::memory, target_type::memory> {};

	using p2m = route<target_type::peripheral, target_type::memory>;
	using m2p = route<target_type::memory, target_type::peripheral>;
	using m2m = route<target_type::memory, target_type::memory>;

	template<bool is_source_peripheral, bool is_destination_peripheral> struct detect_result;

	template<> struct detect_result<true, false> {using type = p2m;};
	template<> struct detect_result<false, true> {using type = m2p;};
	template<> struct detect_result<false, false> {using type = m2m;};

	template<class SourceEndpoint, class DestinationEndpoint> using detect = detect_result<SourceEndpoint::is_peripheral, DestinationEndpoint::is_peripheral>::type;
}

#endif // GD32VF103XX_EPL_DMA_ROUTE_H
