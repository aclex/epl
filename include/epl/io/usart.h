/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_IO_USART_H
#define STM32_CPP_EPL_IO_USART_H

#include <utility>
#include <ratio>
#include <cmath>
#include <cstddef>

#include <epl/util.h>

#include <epl/mcu.h>
#include <epl/gpio.h>

#include <epl/io/usart/pin_signal.h>
#include <epl/io/usart/parity.h>
#include <epl/io/usart/interrupt.h>

#include <epl/direction.h>

namespace stm32
{
	namespace epl
	{
		namespace io
		{
			namespace usart
			{
				template
				<
					std::size_t no,
					typename RXPin,
					typename TXPin = void,
					typename RTSPin = void,
					typename CTSPin = void,
					typename CKPin = void
				>
				class usart
				{
				private:
					typedef mcu::usart<no> port;

					static constexpr std::uint16_t s_9bit_mask = 0x01ff;

				public:
					typedef RXPin rx_pin;
					typedef TXPin tx_pin;
					typedef RTSPin rts_pin;
					typedef CTSPin cts_pin;
					typedef CKPin ck_pin;

					static constexpr std::size_t port_no = no;

					template
					<
						std::size_t baud_rate,
						std::size_t word_length,
						std::size_t stop_bits,
						parity par
					>
					static constexpr void configure() noexcept
					{
						usart::configure<word_length, baud_rate, std::ratio<stop_bits>, par>();
					}

					template
					<
						std::size_t word_length,
						std::size_t baud_rate,
						typename StopBitsRatio,
						parity par
					>
					static constexpr void configure() noexcept
					{
						usart::disable();

						port::clock_bus::template enable_clock<port::clock_flag>();

						port::template set_word_length<word_length>();
						port::template set_parity<par>();
						port::template set_mode<rx_pin, tx_pin>();

						port::template set_stop_bits<StopBitsRatio>();

						if constexpr (port::supports_flow_control())
							port::template set_flow_control<rts_pin, cts_pin>();

						port::template set_overdrive<baud_rate>();

						configure_gpio();

						port::template set_speed<baud_rate>();
					}

					static constexpr void enable() noexcept
					{
						port::enable();
					}

					static constexpr void disable() noexcept
					{
						port::disable();
					}

					static constexpr std::uint16_t read() noexcept
					{
						port::wait_for_ready_to_receive();
						return port::read();
					}

					static void write(std::uint16_t v) noexcept
					{
						port::wait_for_ready_to_send();
						port::write(v);
						port::wait_for_transfer_complete();
					}

					template
					<
						typename VectorType,
						typename std::enable_if<!std::is_integral<VectorType>::value, int>::type i = 0
					>
					static void write(const VectorType& v) noexcept
					{
						for (auto e : v)
						{
							port::wait_for_ready_to_send();
							port::write(e);
						}

						port::wait_for_transfer_complete();
					}

					template<interrupt ir, std::size_t priority = 1> static void set_interrupt_handler(void (* const f)()) noexcept
					{
						port::template set_interrupt_handler<ir, priority>(f);
					}

					template<interrupt ir> static void reset_interrupt_handler() noexcept
					{
						port::template reset_interrupt_handler<ir>();
					}

					usart& operator<<(unsigned char byte)
					{
						write(byte);
						return *this;
					}

					usart& operator<<(char byte)
					{
						write(byte);
						return *this;
					}

					template<class VectorType> usart& operator<<(const VectorType& bytes)
					{
						this->write(bytes);

						return *this;
					}

					usart& operator>>(unsigned char& byte)
					{
						byte = read();
						return *this;
					}

					usart& operator>>(char& byte)
					{
						byte = read();
						return *this;
					}

					template<class VectorType> usart& operator>>(VectorType& bytes)
					{
						for (auto& b : bytes)
						{
							b = this->read();
						}

						return *this;
					}

				private:
					static constexpr void configure_gpio() noexcept
					{
						if constexpr(!std::is_void<rx_pin>::value)
						{
							port::template signal_mapper<pin_signal::rx>::template check<rx_pin::port_name, rx_pin::pin_no>();
							rx_pin::template configure<port::af, gpio::output_type::push_pull,
													  gpio::pull::up, mcu::gpio::speed::high>();
						}

						if constexpr(!std::is_void<tx_pin>::value)
						{
							port::template signal_mapper<pin_signal::tx>::template check<tx_pin::port_name, tx_pin::pin_no>();
							tx_pin::template configure<port::af, gpio::output_type::push_pull,
													  gpio::pull::up, mcu::gpio::speed::high>();
						}

						if constexpr(!std::is_void<rts_pin>::value)
						{
							port::template signal_mapper<pin_signal::rts>::template check<rts_pin::port_name, rts_pin::pin_no>();
							rts_pin::template configure<port::af, gpio::output_type::push_pull,
													  gpio::pull::up, mcu::gpio::speed::high>();
						}

						if constexpr(!std::is_void<cts_pin>::value)
						{
							port::template signal_mapper<pin_signal::cts>::template check<cts_pin::port_name, cts_pin::pin_no>();
							cts_pin::template configure<port::af, gpio::output_type::push_pull,
													  gpio::pull::up, mcu::gpio::speed::high>();
						}

						if constexpr(!std::is_void<ck_pin>::value)
						{
							port::template signal_mapper<pin_signal::ck>::template check<ck_pin::port_name, ck_pin::pin_no>();
							ck_pin::template configure<port::af, gpio::output_type::push_pull,
														gpio::pull::up, mcu::gpio::speed::high>();
						}
					}
				};
			}
		}
	}
}

#endif // STM32_CPP_EPL_IO_USART_H
